from django.contrib.auth import get_user_model
from django.dispatch import Signal
from django.db.models.signals import pre_delete, post_save, post_delete
from django.dispatch import receiver

GitaUser = get_user_model()
friendship_request_created = Signal()
friendship_request_rejected = Signal()
friendship_request_canceled = Signal()
friendship_request_viewed = Signal()
friendship_request_accepted = Signal(providing_args=['from_user', 'to_user'])
friendship_removed = Signal(providing_args=['from_user', 'to_user'])
follower_created = Signal(providing_args=['follower'])
follower_removed = Signal(providing_args=['follower'])
leader_created = Signal(providing_args=['leader'])
leader_removed = Signal(providing_args=['leader'])
following_created = Signal(providing_args=['following'])
following_removed = Signal(providing_args=['following'])
block_created = Signal(providing_args=['blocker'])
block_removed = Signal(providing_args=['blocker'])

# @receiver(post_delete, sender=GitaUser)
# def gita_user_post_delete(sender: GitaUser, instance: GitaUser, **kwargs):
#     Username.super_objects.get(username_value=instance.username.username_value).delete()

# @receiver(post_save, sender=GitaUser)
# def gita_user_post_save(sender: GitaUser, instance: GitaUser, created: bool, **kwargs):
#     if created:
#         Username.objects.create(username_value=instance.username)
