import string
from datetime import timedelta
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from rest_framework import permissions, generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from core_api.serializers.registration import (
    UserRegisterSerializer, CheckUsernameRegisterSerializer,
    ResendPhoneTokenSerializer, VerifyPhoneSerializer)
from social_network import sms_gateway
from social_network.models.verification import UserPhoneVerificationToken

User = get_user_model()


class CheckUsernameRegisterAPIView(generics.CreateAPIView):
    serializer_class = CheckUsernameRegisterSerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response_data = serializer.validated_data
        response_data['is_valid'] = not User.objects.filter(
            username=serializer.validated_data['username'],
            phone_verified__isnull=False
        ).exists()
        return Response(response_data, status=status.HTTP_200_OK)


class UserRegisterAPIView(generics.CreateAPIView):
    serializer_class = UserRegisterSerializer
    permission_classes = (permissions.AllowAny,)

    def perform_create(self, serializer):
        user = serializer.save()
        if UserPhoneVerificationToken.objects.filter(user=user).exists():
            if now() - timedelta(
                    minutes=settings.SMS_REGISTRATION_MINIMUM_INTERVAL_MINUTE
            ) <= user.phone_verification_token.created_at:
                time_left = user.phone_verification_token.created_at - (
                        now() - timedelta(minutes=settings.SMS_REGISTRATION_MINIMUM_INTERVAL_MINUTE)
                )
                time_left_human = divmod(time_left.days * 86400 + time_left.seconds, 60)
                msg = _('Please wait {} second{} ({} minute{} {} second{}) before request another token.')
                msg = msg.format(
                    time_left.seconds,
                    "s" if time_left.seconds > 1 else "",
                    time_left_human[0],
                    "s" if time_left_human[0] > 1 else "",
                    time_left_human[1],
                    "s" if time_left_human[1] > 1 else ""
                )
                raise ValidationError(msg, code="sms_registration_minimum_interval_pending")
            UserPhoneVerificationToken.objects.filter(user=user).delete()
        token = User.objects.make_random_password(6, string.digits)
        user_phone_verification = UserPhoneVerificationToken(user=user, token=token)
        if settings.IS_ONLINE:
            message = f"Gita code {token}"
            sms_gateway.send(message, [user.phone])
        user_phone_verification.save()


class ResendPhoneTokenAPIView(generics.CreateAPIView):
    serializer_class = ResendPhoneTokenSerializer
    permission_classes = (permissions.AllowAny,)

    def perform_create(self, serializer):
        if not User.objects.filter(phone=serializer.validated_data['phone']).exists():
            msg = _('User with this phone number not registered yet.')
            raise ValidationError(msg, code='phone_not_registered')
        user = User.objects.get(phone=serializer.validated_data['phone'])
        if user.phone_verified:
            msg = _('This phone number has been verified.')
            raise ValidationError(msg, code='phone_already_verified')
        if UserPhoneVerificationToken.objects.filter(user=user).exists():
            if now() - timedelta(
                    minutes=settings.SMS_REGISTRATION_MINIMUM_INTERVAL_MINUTE
            ) <= user.phone_verification_token.created_at:
                time_left = user.phone_verification_token.created_at - (
                        now() - timedelta(minutes=settings.SMS_REGISTRATION_MINIMUM_INTERVAL_MINUTE)
                )
                time_left_human = divmod(time_left.days * 86400 + time_left.seconds, 60)
                msg = _("Please wait {} second{} ({} minute{} {} second{}) before request another token.")
                msg = msg.format(
                    time_left.seconds,
                    "s" if time_left.seconds > 1 else "",
                    time_left_human[0],
                    "s" if time_left_human[0] > 1 else "",
                    time_left_human[1],
                    "s" if time_left_human[1] > 1 else ""
                )
                raise ValidationError(msg, code="sms_registration_minimum_interval_pending")
            else:
                UserPhoneVerificationToken.objects.filter(user=user).delete()
                token = User.objects.make_random_password(6, string.digits)
                user_phone_verification = UserPhoneVerificationToken(user=user, token=token)
                if settings.IS_ONLINE:
                    message = f"Gita token new {token}"
                    sms_gateway.send(message, [user.phone])
                user_phone_verification.save()


class VerifyPhoneRegisterAPIView(generics.CreateAPIView):
    serializer_class = VerifyPhoneSerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not User.objects.filter(phone=serializer.validated_data['phone']).exists():
            msg = _('User with this phone number not registered yet.')
            raise ValidationError(msg, code='phone_not_registered')
        user = User.objects.get(phone=serializer.validated_data['phone'])
        if user.phone_verified:
            msg = _('This phone number has been verified.')
            raise ValidationError(msg, code='phone_already_verified')

        token = serializer.validated_data['phone_token']
        if UserPhoneVerificationToken.objects.filter(user=user, token=token).exists():
            phone_verification = UserPhoneVerificationToken.objects.get(user=user, token=token)
            if now() - settings.VERIFY_PHONE_TOKEN_EXPIRATION_DELTA > phone_verification.created_at:
                msg = 'Token expired.'
                raise ValidationError(msg, code='phone_verification_token_expired')
            response_data = {'phone_key': phone_verification.uuid.hex}
            user.phone_verified = now()
            user.save()
            return Response(response_data, status=status.HTTP_200_OK)
        msg = _("Invalid token.")
        raise ValidationError(msg, code='phone_verification_token_invalid')
