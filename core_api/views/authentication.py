from django.contrib.auth import get_user_model
from rest_framework import status, permissions, views
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from core_api.serializers.authentication import LoginSerializer
from social_network.serializers.profile import UserSerializer

User = get_user_model()


class LoginAPIView(ObtainAuthToken):
    serializer_class = LoginSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        user_serializer = UserSerializer(instance=user, context={'request': self.request})
        return Response({'token': token.key, 'user': user_serializer.data})


class VerifyAPIView(views.APIView):
    def post(self, request, *args, **kwargs):
        user_serializer = UserSerializer(instance=self.request.user, context={'request': self.request})
        return Response(
            {'token': request.user.auth_token.key, 'user': user_serializer.data}
        )


class LogoutAPIView(views.APIView):
    def post(self, request, *args, **kwargs):
        request.user.auth_token.delete()
        return Response({}, status.HTTP_200_OK)
