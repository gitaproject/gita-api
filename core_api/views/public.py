from rest_framework import permissions, generics

from rest_framework.response import Response


class FAQAPIView(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)

    def retrieve(self, request, *args, **kwargs):
        data_result = {}
        return Response(data_result)


class TOSAPIView(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)

    def retrieve(self, request, *args, **kwargs):
        data_result = {}
        return Response(data_result)
