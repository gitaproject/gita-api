import string

from django.conf import settings
from django.contrib.auth import get_user_model, password_validation
from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.validators import validate_email
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.validators import validate_international_phonenumber
from rest_framework import permissions, generics, exceptions, status, serializers
from rest_framework.response import Response

from core_api.serializers.account_retrieval import RecoverySerializer, ValidateRecoverySerializer, \
    ResetPasswordSerializer
from gita_emails.mails.account_retrieval import AccountRetrievalMailer
from social_network import sms_gateway
from social_network.models.verification import UserForgotPasswordToken

User = get_user_model()


class RecoveryAPIView(generics.CreateAPIView):
    serializer_class = RecoverySerializer
    permission_classes = (permissions.AllowAny,)

    def perform_create(self, serializer):
        username = serializer.validated_data['username']
        try:
            validate_email(username)
        except DjangoValidationError:
            try:
                validate_international_phonenumber(username)
            except DjangoValidationError:
                kwargs = {'username': username}
            else:
                kwargs = {'phone': username}
        else:
            kwargs = {'email': username}

        if not User.objects.filter(**kwargs).exists():
            raise exceptions.ValidationError(
                _('Username does not match to any user data.'),
                'recovery_username_invalid'
            )
        user = User.objects.get(**kwargs)
        self.send_recovery_url(user)

    def send_recovery_url(self, user):
        if UserForgotPasswordToken.objects.filter(user=user).exists():
            if now() - settings.ACCOUNT_RETRIEVAL_INTERVAL_DELTA <= user.phone_verification_token.created_at:
                time_left = user.phone_verification_token.created_at - (
                        now() - settings.ACCOUNT_RETRIEVAL_INTERVAL_DELTA
                )
                time_left_human = divmod(time_left.days * 86400 + time_left.seconds, 60)
                msg = _('Please wait {} second{} ({} minute{} {} second{}) before another request.')
                msg.format(
                    time_left.seconds,
                    "s" if time_left.seconds > 1 else "",
                    time_left_human[0],
                    "s" if time_left_human[0] > 1 else "",
                    time_left_human[1],
                    "s" if time_left_human[1] > 1 else ""
                )
                raise exceptions.ValidationError(msg, code="account_recovery_minimum_interval_pending")
            else:
                UserForgotPasswordToken.objects.filter(user=user).delete()
        token = User.objects.make_random_password(120, string.ascii_letters + string.digits)
        user_forgot_password = UserForgotPasswordToken.objects.create(user=user, token=token)
        recovery_url = f"{settings.WEB_URL}recovery/" \
            f"validate/{user_forgot_password.uuid.hex}/{user_forgot_password.token}/"
        if user.email_verified:
            try:
                if settings.IS_ONLINE:
                    mailer = AccountRetrievalMailer(request=self.request)
                    mailer.send_recovery_url(user_forgot_password, recovery_url)
            except Exception:
                raise exceptions.APIException(_('Fail to send account retrieval mail.'))
        elif user.phone_verified:
            try:
                if settings.IS_ONLINE:
                    message = f"Please go to this link to reset your GiTa account password: {recovery_url}"
                    sms_gateway.send(message, [user.phone])
            except Exception:
                raise exceptions.APIException(_('Fail to send account retrieval SMS.'))
        else:
            raise exceptions.ValidationError(_('User registration incomplete.'), 'account_recovery_user_unverified')


class RecoveryValidationAPIView(generics.CreateAPIView):
    serializer_class = ValidateRecoverySerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        uuid = serializer.validated_data['uuid']
        token = serializer.validated_data['token']
        is_valid = UserForgotPasswordToken.objects.filter(uuid=uuid, token=token).exists()
        data_result = {'is_valid': is_valid}
        headers = self.get_success_headers(serializer.data)
        return Response(
            data_result,
            status=status.HTTP_200_OK if is_valid else status.HTTP_422_UNPROCESSABLE_ENTITY,
            headers=headers
        )


class RecoverySetPasswordAPIView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ResetPasswordSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        uuid = serializer.validated_data['uuid']
        token = serializer.validated_data['token']
        is_valid = UserForgotPasswordToken.objects.filter(uuid=uuid, token=token).exists()
        headers = self.get_success_headers(serializer.data)
        if not is_valid:
            return Response(
                {},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                headers=headers
            )
        user_forgot_password = UserForgotPasswordToken.objects.get(uuid=uuid, token=token)
        user = user_forgot_password.user
        new_password = serializer.validated_data['password']
        try:
            password_validation.validate_password(password=new_password, user=user)
        except DjangoValidationError as e:
            errors = serializers.as_serializer_error(e)
            raise serializers.ValidationError(detail=errors)
        else:
            user.set_password(new_password)
            user.save()
            return Response({}, status=status.HTTP_200_OK, headers=headers)
