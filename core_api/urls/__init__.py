from django.conf import settings
from django.urls import path, include

urlpatterns = [
    path('public/', include('core_api.urls.public'), name='public'),
    path('auth/', include('core_api.urls.authentication'), name='authentication'),
    path('register/', include('core_api.urls.registration'), name='registration'),
    path('account_retrieval/', include('core_api.urls.account_retrieval'), name='account_retrieval'),

    path('agenda/', include('agenda.urls'), name='agenda'),
    path('chat/', include('chat.urls'), name='chat'),
    path('notification/', include('notification.urls'), name='notification'),
    path('relationship/', include('relationship.urls'), name='relationship'),
    path('social/', include('social_network.urls'), name='social'),

]

if settings.DEBUG:
    urlpatterns += [
        path('echo/', include('core_api.urls.echo'), name='echo'),
    ]
