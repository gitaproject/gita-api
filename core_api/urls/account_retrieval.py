from django.urls import path

from core_api.views.account_retrieval import (
    RecoveryAPIView, RecoveryValidationAPIView, RecoverySetPasswordAPIView
)

urlpatterns = [
    path('recovery/', RecoveryAPIView.as_view(), name='account_recovery'),
    path('validate-recovery/', RecoveryValidationAPIView.as_view(), name='validate_account_retrieval'),
    path('set-password/', RecoverySetPasswordAPIView.as_view(), name='recovery_set_password'),
]
