from django.contrib.auth import get_user_model
from django.db import models

from .base import BaseAbstractModel

User = get_user_model()


class AbstractComments(BaseAbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_by')
    comment = models.CharField(max_length=450)
    is_edited = models.BooleanField(default=False)

    class Meta:
        abstract = True
