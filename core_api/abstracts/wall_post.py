from django.contrib.auth import get_user_model
from django.db import models

from .base import BaseAbstractModel

User = get_user_model()


class AbstractWallPost(BaseAbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_by')
    body = models.CharField(max_length=900)
    is_edited = models.BooleanField(default=False)

    class Meta:
        abstract = True
        ordering = ['-created_at']
