from django.contrib.auth import get_user_model
from django.db import models

from .base import BaseAbstractModel

User = get_user_model()


class AbstractLikes(BaseAbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_by')

    class Meta:
        abstract = True
