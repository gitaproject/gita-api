from datetime import timedelta, datetime

from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from sorl.thumbnail import ImageField as ThumbnailImageField

from core_api.abstracts.base import BaseAbstractModel
from core_api.abstracts.username import UsernameAbstractModel
from core_api.caches import cache_key
from social_network.managers.location import LocationProfileManager, LocationAdminsManager

User = get_user_model()


def location_avatar_directory_path(instance, filename):
    return 'location_{0}/avatar/{1}'.format(instance.id, filename)


def location_cover_directory_path(instance, filename):
    return 'location_{0}/cover/{1}'.format(instance.id, filename)


class LocationProfile(UsernameAbstractModel):
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    location_name = models.CharField(max_length=55)
    profile_picture = ThumbnailImageField(
        upload_to=location_avatar_directory_path,
        default='images/default_location_avatar.jpeg'
    )
    profile_cover = models.ImageField(
        upload_to=location_cover_directory_path,
        default='images/default_location_cover.jpeg'
    )
    about = models.TextField(max_length=255, blank=True, default='')
    objects = LocationProfileManager()

    class Meta:
        ordering = ['-created_at']

    def save(self, *args, **kwargs):
        is_new = not self.pk
        super().save(*args, **kwargs)
        if is_new:
            location_admin = LocationAdmins.objects.create(location=self, admin=self.creator)
            key = cache_key('locations', self.pk)
            cache.set(key, (self,))
            key = cache_key('location_admins', self.pk)
            cache.set(key, (location_admin,))

    def add_admin(self, admin: User):
        """ add admin to this LocationProfile object"""
        location_admin, created = LocationAdmins.objects.get_or_create(location=self, admin=admin)
        key = cache_key('location_admins', self.pk)
        cache.set(key, None)
        # location_admins = cache.get(key)
        # if created and location_admins:
        #     cache.set(key, location_admins.append(location_admin))

    def is_admin(self, admin: User):
        """ check if user is admin of this LocationProfile object"""
        key = cache_key('location_admins', self.pk)
        location_admins = cache.get(key)
        if location_admins is None:
            location_admins = LocationAdmins.objects.select_related('location').filter(
                location=self).all().order_by('-id')
            cache.set(key, location_admins)
        return location_admins.filter(admin=admin).exists()

    def location_admins(self):
        """ Return a list of all admins of this location """
        key = cache_key('location_admins', self.pk)
        location_admins = cache.get(key)
        if location_admins is None:
            location_admins = self.objects.select_related('location').filter(location=self).all().order_by('-id')
            cache.set(key, location_admins)
        return location_admins

    def get_close_time(self, _type, _time):
        if not LocationOpenTime.objects.filter(location=self).exists():
            return []
        if self.time.open_loop == LocationTime.NO_LOOP:
            return []
        if self.time.open_loop == LocationTime.DAILY:
            time_start = datetime.fromtimestamp(_time[1])
            first_start = (
                    time_start - timedelta(days=time_start.weekday())
            ).replace(
                hour=0, minute=0, second=0, microsecond=0
            ).timestamp()
            close_times = []
            last_close = 316800
            for open_time in self.open_times.all().order_by('in_loop', 'time_start'):
                open_start = open_time.time_start + (open_time.in_loop * 86400) + 316800
                open_until = open_time.time_until + (open_time.in_loop * 86400) + 316800
                if open_start <= last_close:
                    last_close = open_until
                    continue
                close_times.append([last_close, open_start])
                last_close = open_until
            close_times.append([last_close, 921599])
            return [
                [
                    close_time[0] + first_start + (604800 * (i * _type[1])),
                    close_time[1] + first_start + (604800 * (i * _type[1]))
                ] for close_time in close_times for i in range(_time[0])
            ]
        if self.time.open_loop == LocationTime.WEEKLY:
            return []
        if self.time.open_loop == LocationTime.MONTHLY:
            return []
        if self.time.open_loop == LocationTime.YEARLY:
            return []
        return []


class LocationAdmins(BaseAbstractModel):
    location = models.ForeignKey(LocationProfile, on_delete=models.CASCADE, related_name='admins')
    admin = models.ForeignKey(User, models.CASCADE, related_name='administrated_locations')
    objects = LocationAdminsManager()

    def __str__(self):
        return f'admin {self.admin} of {self.location.location_name}'


class LocationTime(BaseAbstractModel):
    NO_LOOP = 'nl'
    DAILY = 'da'
    WEEKLY = 'we'
    MONTHLY = 'mo'
    YEARLY = 'ye'
    LOOP_TYPE = (
        (NO_LOOP, 'Always Open'),
        (DAILY, 'Daily'),
        (WEEKLY, 'Weekly'),
        (MONTHLY, 'Monthly'),
        (YEARLY, 'Yearly'),
    )
    location = models.OneToOneField(LocationProfile, on_delete=models.CASCADE, related_name='time')
    open_loop = models.CharField(max_length=2, choices=LOOP_TYPE, default=DAILY)
    open_interlude = models.PositiveIntegerField(default=1)


class LocationOpenTime(BaseAbstractModel):
    location = models.ForeignKey(LocationProfile, on_delete=models.CASCADE, related_name='open_times')
    in_loop = models.PositiveIntegerField()
    time_start = models.PositiveIntegerField(default=0)
    time_until = models.PositiveIntegerField(default=0)

    def save(self, *args, **kwargs):
        if self.time_start >= self.time_until:
            raise ValidationError(f"Range time not valid {self.time_start} - {self.time_until}")
        if self.location.time.open_loop == LocationTime.DAILY:
            super().save(*args, **kwargs)
