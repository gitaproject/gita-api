import uuid as uuid
from django.contrib.auth import get_user_model
from django.db import models

from core_api.abstracts.base import BaseAbstractModel

User = get_user_model()


class UserForgotPasswordToken(BaseAbstractModel):
    user = models.OneToOneField(User, models.CASCADE, related_name='change_password_token')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    token = models.CharField(max_length=120)


class UserEmailVerificationToken(BaseAbstractModel):
    user = models.OneToOneField(User, models.CASCADE, related_name='email_verification_token')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    token = models.CharField(max_length=10)


class UserPhoneVerificationToken(BaseAbstractModel):
    user = models.OneToOneField(User, models.CASCADE, related_name='phone_verification_token')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    token = models.CharField(max_length=10)
