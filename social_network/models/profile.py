from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField as ThumbnailImageField

from core_api.abstracts.base import BaseAbstractModel

User: AbstractUser = get_user_model()


def user_avatar_directory_path(instance, filename):
    return 'user_{0}/avatar/{1}'.format(instance.user.id, filename)


def user_canvas_directory_path(instance, filename):
    return 'user_{0}/canvas/{1}'.format(instance.user.id, filename)


class UserProfile(BaseAbstractModel):
    NONE = 'N'
    FEMALE = 'F'
    MALE = 'M'
    OTHER = 'O'
    SEX_CHOICES = (
        (NONE, _('Rather not say')),
        (FEMALE, _('Female')),
        (MALE, _('Male')),
        (OTHER, _('Other')),
    )
    user = models.OneToOneField(User, models.CASCADE, related_name='profile')
    birthday = models.DateField()
    gender = models.CharField(max_length=1, choices=SEX_CHOICES, default=NONE)
    about = models.TextField(max_length=255, blank=True, default='')
    avatar = ThumbnailImageField(
        upload_to=user_avatar_directory_path,
        default='images/default_user_avatar.jpeg'
    )
    cover = models.ImageField(
        upload_to=user_canvas_directory_path,
        default='images/default_user_cover.jpeg'
    )

    def __str__(self):
        return f'{self.user} profile'
