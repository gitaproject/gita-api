from django.contrib.auth import get_user_model
from django.db import models

from core_api.abstracts.base import BaseAbstractModel

User = get_user_model()


class Posts(BaseAbstractModel):
    USER_ACTIVITY_POSTED = 1  # personal user activity
    FRIEND_ACTIVITY_POSTED = 2
    AGENDA_CREATED = 3
    AGENDA_UPDATED = 4
    ACTIVITY_COMMENTED = 5
    ACTIVITY_LIKED = 6
    USER_STATUS = 7
    FRIEND_STATUS = 8
    USER_WALL = 9
    FRIEND_WALL = 10
    GROUP_WALL = 11
    LOCATION_WALL = 12
    USER_RELATIONSHIP = 13
    FRIEND_RELATIONSHIP = 14

    POST_TYPE = (
        (USER_ACTIVITY_POSTED, 'user activity posted'),
        (FRIEND_ACTIVITY_POSTED, 'friend activity posted'),
        (AGENDA_CREATED, 'agenda created'),
        (AGENDA_UPDATED, 'agenda updated'),
        (ACTIVITY_COMMENTED, 'activity commented'),
        (ACTIVITY_LIKED, 'activity liked'),
        (USER_STATUS, 'user status'),
        (FRIEND_STATUS, 'friend status'),
        (USER_WALL, 'user wall'),
        (FRIEND_WALL, 'friend wall'),
        (GROUP_WALL, 'group wall'),
        (LOCATION_WALL, 'location wall'),
        (USER_RELATIONSHIP, 'user relationship'),
        (FRIEND_RELATIONSHIP, 'friend relationship'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    post_type = models.IntegerField(choices=POST_TYPE)
    post_related_id = models.IntegerField()

    class Meta:
        ordering = ['-created_at']
        get_latest_by = "created_at"

    @property
    def post_related_object(self):
        obj = None
        return obj


class Feed(Posts):
    class Meta:
        ordering = ['-created_at']
        get_latest_by = "created_at"
        proxy = True
