from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class SMSHistory(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    error_code = models.CharField(max_length=55, default=None, null=True, blank=True)
    error = models.CharField(max_length=55, default=None, null=True, blank=True)
    error_description = models.TextField(default=None, null=True, blank=True)


class SMSReceivers(models.Model):
    sms_history = models.ForeignKey(SMSHistory, models.CASCADE, 'receivers')
    phone = PhoneNumberField()
    api_message_id = models.CharField(max_length=55, default=None, null=True)
    accepted = models.BooleanField(default=None, null=True)
    error_code = models.CharField(max_length=55, default=None, null=True, blank=True)
    error = models.CharField(max_length=55, default=None, null=True, blank=True)
    error_description = models.TextField(default=None, null=True, blank=True)
