from django.contrib.auth import get_user_model

from social_network.serializers.profile import OtherUserSerializer

User = get_user_model()


class ExploreSerializer(OtherUserSerializer):
    pass
