from rest_framework import serializers

from social_network.models.walls import LocationWallPostLikes, GroupWallPostLikes, WallPostLikes
from social_network.serializers.profile import OtherUserSerializer


class WallLikeSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = WallPostLikes
        fields = ('wall_post', 'created_at', 'user',)


class GroupWallLikeSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = GroupWallPostLikes
        fields = ('wall_post', 'created_at', 'user',)


class LocationWallLikeSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = LocationWallPostLikes
        fields = ('wall_post', 'created_at', 'user',)
