from django.contrib.auth import get_user_model
from rest_framework import serializers

from social_network.models.group import GroupProfile, GroupMembers
from social_network.models.profile import UserProfile
from social_network.serializers.profile import OtherUserSerializer

User = get_user_model()


class GroupSerializer(serializers.ModelSerializer):
    creator = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = GroupProfile
        fields = (
            'id', 'username', 'group_name', 'about', 'profile_picture', 'profile_cover', 'creator', 'created_at',
            'updated_at'
        )
        read_only_fields = ('id', 'creator', 'created_at', 'updated_at')


class UserGroupSerializer(serializers.ModelSerializer):
    group = GroupSerializer(many=False)

    class Meta:
        model = GroupMembers
        fields = ('group', 'join_date',)
        read_only_fields = ('join_date',)


class GroupMemberAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMembers
        fields = ('id', 'group', 'member',)

    def create(self, validated_data):
        if GroupMembers.objects.filter(
                admin__notnull=True, group=validated_data['group'],
                admin=self.context['request'].user
        ).exists():
            return super().create(validated_data)
        else:
            raise serializers.ValidationError('You do not have permission')


class GroupMemberUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('about', 'avatar', 'gender',)


class GroupMemberUserSerializer(serializers.ModelSerializer):
    profile = GroupMemberUserProfileSerializer(many=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'profile',)


class GroupMemberSerializer(GroupMemberAddSerializer):
    member = GroupMemberUserSerializer(many=False)

    class Meta:
        model = GroupMembers
        fields = ('id', 'member', 'join_date',)


class GroupMembersManageSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMembers
        fields = ('id', 'member', 'group', 'join_date',)
        read_only_fields = ('id', 'join_date', 'group',)


class GroupAdminManageSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMembers
        fields = ('id', 'member',)
