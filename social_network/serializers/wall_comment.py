from rest_framework import serializers

from social_network.models.walls import WallPostComment, GroupWallPostComment, LocationWallPostComment
from social_network.serializers.profile import OtherUserSerializer


class WallCommentSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)
    is_edited = serializers.BooleanField(read_only=True)

    class Meta:
        model = WallPostComment
        fields = ('id', 'wall_post', 'created_at', 'updated_at', 'user', 'comment', 'is_edited',)


class GroupWallCommentSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)
    is_edited = serializers.BooleanField(read_only=True)

    class Meta:
        model = GroupWallPostComment
        fields = ('id', 'wall_post', 'created_at', 'updated_at', 'user', 'comment', 'is_edited',)


class LocationWallCommentSerializer(serializers.ModelSerializer):
    user = OtherUserSerializer(many=False, read_only=True)
    is_edited = serializers.BooleanField(read_only=True)

    class Meta:
        model = LocationWallPostComment
        fields = ('id', 'wall_post', 'created_at', 'updated_at', 'user', 'comment', 'is_edited',)
