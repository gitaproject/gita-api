from django.contrib.auth import get_user_model
from rest_framework import serializers

from social_network.models.location import LocationProfile, LocationAdmins
from social_network.serializers.profile import OtherUserSerializer

User = get_user_model()


class LocationSerializer(serializers.ModelSerializer):
    creator = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = LocationProfile
        fields = (
            'id', 'username', 'location_name', 'about', 'profile_picture', 'profile_cover', 'creator', 'created_at',
            'updated_at'
        )
        read_only_fields = ('id', 'creator', 'created_at', 'updated_at')


class UserLocationSerializer(serializers.ModelSerializer):
    location = LocationSerializer(many=False)

    class Meta:
        model = LocationAdmins
        fields = ('location', 'updated_at', 'created_at',)
        read_only_fields = ('updated_at', 'created_at',)
