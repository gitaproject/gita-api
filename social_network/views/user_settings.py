from rest_framework import generics
from social_network.serializers.user_settings import GeneralSettingSerializer, SecuritySettingSerializer


class GeneralSettingAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = GeneralSettingSerializer
    permission_classes = (IsActivePermission,)

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)


class SecuritySettingAPIView(generics.RetrieveAPIView):
    serializer_class = SecuritySettingSerializer
    permission_classes = (IsActivePermission,)
    lookup_field = 'user__username'
    lookup_url_kwarg = 'username'

    def get_object(self):
        return UserProfile.objects.get(user__username=self.kwargs.get('username'))
