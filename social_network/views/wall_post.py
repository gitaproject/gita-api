from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions

from social_network.models.walls import WallPost, LocationWallPost, GroupWallPost
from core_api.permissions.wall import (
    IsOwnerWallPostAndFriendOfUserAndAuthenticatedUser, IsOwnerWallPostAndMemberOfGroupAndAuthenticatedUser,
    IsOwnerWallPostAndAdminOfLocationAndAuthenticatedUser
)
from social_network.serializers.wall import WallSerializer, GroupWallSerializer

User = get_user_model()


class UserWallPostDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WallSerializer
    permission_classes = (IsOwnerWallPostAndFriendOfUserAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'post_id'

    def get_object(self):
        if not WallPost.objects.filter(id=self.kwargs.get('post_id')).exists():
            raise exceptions.ValidationError(
                _('Wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'detail_wall_post_visit_invalid_post_id'
            )

        obj = WallPost.objects.get(id=self.kwargs.get('post_id'))
        self.check_object_permissions(self.request, obj)
        return obj


class GroupWallPostDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GroupWallSerializer
    permission_classes = (IsOwnerWallPostAndMemberOfGroupAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'post_id'

    def get_object(self):
        if not GroupWallPost.objects.filter(id=self.kwargs.get('post_id')).exists():
            raise exceptions.ValidationError(
                _('Group wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'detail_wall_post_group_invalid_post_id'
            )

        obj = GroupWallPost.objects.get(id=self.kwargs.get('post_id'))
        self.check_object_permissions(self.request, obj)
        return obj


class LocationWallPostDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WallSerializer
    permission_classes = (IsOwnerWallPostAndAdminOfLocationAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'post_id'

    def get_object(self):
        if not LocationWallPost.objects.filter(id=self.kwargs.get('post_id')).exists():
            raise exceptions.ValidationError(
                _('Location wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'detail_wall_post_location_invalid_post_id'
            )

        obj = LocationWallPost.objects.get(id=self.kwargs.get('post_id'))
        self.check_object_permissions(self.request, obj)
        return obj
