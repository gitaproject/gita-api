from django.contrib.auth import get_user_model
from rest_framework import generics

from core_api import permissions
from social_network.serializers.explore import ExploreSerializer

User = get_user_model()


class ExploreAPIView(generics.ListAPIView):
    queryset = User.objects.all().order_by('username')
    serializer_class = ExploreSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    search_fields = ('username', 'first_name', 'last_name', 'profile__about',)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.exclude(id=self.request.user.id)
        # TODO: exclude private accounts
        return queryset
