from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions

from social_network.models import (
    GroupProfile, LocationProfile, WallPost, GroupWallPost, LocationWallPost,
    WallPostLikes, GroupWallPostLikes, LocationWallPostLikes)
from core_api.permissions import IsDefaultAuthenticatedUser
from core_api.permissions.wall import (
    IsMemberOfGroupAndAuthenticatedUser, IsFriendOfUserAndAuthenticatedUser
)
from social_network.serializers.wall_like import (
    WallLikeSerializer, GroupWallLikeSerializer, LocationWallLikeSerializer,
)

User = get_user_model()


class UserWallLikeAPIView(generics.ListCreateAPIView):
    serializer_class = WallLikeSerializer
    permission_classes = (IsFriendOfUserAndAuthenticatedUser,)

    def get_queryset(self):
        wall_post = self.get_object()
        return WallPostLikes.objects.filter(wall_post=wall_post).order_by('-created_at')

    def perform_create(self, serializer):
        wall_post = self.get_object()
        if WallPostLikes.objects.filter(wall_post=wall_post, user=self.request.user).exists():
            WallPostLikes.objects.get(wall_post=wall_post, user=self.request.user).delete()
        else:
            serializer.save(
                wall_post=wall_post,
                user=self.request.user
            )

    def get_object(self) -> WallPost:
        if not WallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'list_create_wall_post_user_like_invalid_post_id'
            )
        obj = WallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj.wall_of_user)
        return obj


class GroupWallLikeAPIView(generics.ListCreateAPIView):
    serializer_class = GroupWallLikeSerializer
    permission_classes = (IsMemberOfGroupAndAuthenticatedUser,)

    def get_queryset(self):
        wall_post = self.get_object()
        return GroupWallPostLikes.objects.filter(wall_post=wall_post).order_by('-created_at')

    def perform_create(self, serializer):
        wall_post = self.get_object()
        if GroupWallPostLikes.objects.filter(wall_post=wall_post, user=self.request.user).exists():
            GroupWallPostLikes.objects.get(wall_post=wall_post, user=self.request.user).delete()
        else:
            serializer.save(
                wall_post=wall_post,
                user=self.request.user
            )

    def get_object(self) -> GroupWallPost:
        if not GroupWallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Group wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'list_create_wall_post_group_like_invalid_post_id'
            )
        obj = GroupWallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj.wall_of_group)
        return obj


class LocationWallLikeAPIView(generics.ListCreateAPIView):
    serializer_class = LocationWallLikeSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        wall_post = self.get_object()
        return LocationWallPostLikes.objects.filter(wall_post=wall_post).order_by('-created_at')

    def perform_create(self, serializer):
        wall_post = self.get_object()
        if LocationWallPostLikes.objects.filter(wall_post=wall_post, user=self.request.user).exists():
            LocationWallPostLikes.objects.get(wall_post=wall_post, user=self.request.user).delete()
        else:
            serializer.save(
                wall_post=wall_post,
                user=self.request.user
            )

    def get_object(self) -> LocationWallPost:
        if not LocationWallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Location wall post with id {} does not exist').format(self.kwargs.get('post_id')),
                'list_create_wall_post_location_like_invalid_post_id'
            )
        obj = LocationWallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj.wall_of_user)
        return obj
