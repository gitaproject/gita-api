from django.db.models import Q
from rest_framework import generics

from core_api import permissions
from relationship.models import Friendship
from social_network.models.posts import Feed
from social_network.serializers.feed import FeedSerializer


class FeedListAPIView(generics.ListAPIView):
    serializer_class = FeedSerializer
    permission_classes = (permissions.IsFeedOwnerAndAuthenticatedUser,)

    def get_queryset(self):
        friends = Friendship.objects.get_friends(self.request.user)
        return Feed.objects.filter(
            Q(user__in=[friend.from_user for friend in friends]) | Q(user=self.request.user)
        )
