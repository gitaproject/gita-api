from rest_framework import generics
from rest_framework.exceptions import PermissionDenied

from core_api import permissions
from social_network.models.location import LocationAdmins, LocationProfile
from social_network.serializers.location import (
    UserLocationSerializer, LocationSerializer
)


class UserGroupListAPIView(generics.ListAPIView):
    serializer_class = UserLocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return LocationAdmins.objects.user_groups(self.request.user)


class LocationAPIView(generics.ListAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    search_fields = ('location_name', 'about')
    queryset = LocationProfile.objects.all().order_by('-location_name')


class LocationCreateAPIView(generics.CreateAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class UserLocationPersonalListAPIView(generics.ListAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return LocationProfile.objects.administered_locations(self.request.user)


class LocationManageAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'location_id'

    def get_object(self):
        if LocationAdmins.objects.filter(
                location=self.kwargs.get('location_id'),
                admin=self.request.user
        ).exists():
            return LocationProfile.objects.get(id=self.kwargs.get('location_id'))
        raise PermissionDenied()

    def perform_destroy(self, instance):
        if instance.creator.pk != self.request.user.pk:
            raise PermissionDenied()
        instance.delete()


class LocationDetailAPIView(generics.RetrieveAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'location_id'

    def get_object(self):
        return LocationProfile.objects.get(id=self.kwargs.get('location_id'))


# class LocationMemberAPIView(generics.ListAPIView):
#     serializer_class = LocationMemberSerializer
#     permission_classes = (permissions.IsDefaultAuthenticatedUser,)
#     search_fields = ('member__first_name', 'member__last_name', 'member__username',)
#
#     def get_queryset(self):
#         return LocationMembers.objects.filter(location=self.kwargs['location_id']).order_by('member__username')


class LocationAdminPromoteAPIView(generics.RetrieveAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'location_id'

    def get_object(self):
        return LocationProfile.objects.get(id=self.kwargs.get('location_id'))


class LocationAdminDemoteAPIView(generics.RetrieveAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'location_id'

    def get_object(self):
        return LocationProfile.objects.get(id=self.kwargs.get('location_id'))
