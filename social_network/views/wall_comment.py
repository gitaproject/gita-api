from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions

from social_network.models import (
    WallPostComment, WallPost, GroupWallPostComment, GroupWallPost, LocationWallPostComment, LocationWallPost,
)
from core_api.permissions import IsDefaultAuthenticatedUser
from core_api.permissions.wall import (
    IsMemberOfGroupAndAuthenticatedUser, IsFriendOfUserAndAuthenticatedUser
)
from core_api.permissions.wall_comment import (
    IsOwnerOrFriendOfWallPostCommentAndAuthenticatedUser,
    IsOwnerOfWallPostCommentOrMemberOfGroupAndAuthenticatedUser,
    IsOwnerWallPostCommentOrAdminOfLocationAndAuthenticatedUser,
)
from social_network.serializers.wall_comment import (
    WallCommentSerializer, GroupWallCommentSerializer, LocationWallCommentSerializer,
)

User = get_user_model()


class UserWallCommentAPIView(generics.ListCreateAPIView):
    serializer_class = WallCommentSerializer
    permission_classes = (IsFriendOfUserAndAuthenticatedUser,)

    def get_queryset(self):
        return WallPostComment.objects.filter(
            wall_post=self.get_object()
        )

    def perform_create(self, serializer):
        serializer.save(wall_post=self.get_object(), user=self.request.user)

    def get_object(self):
        if not WallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Post with this id does not exists.'), 'create_read_wall_post_comment_user_invalid_post_id'
            )
        obj = WallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj.wall_of_user)
        return obj


class GroupWallCommentAPIView(generics.ListCreateAPIView):
    serializer_class = GroupWallCommentSerializer
    permission_classes = (IsMemberOfGroupAndAuthenticatedUser,)

    def get_queryset(self):
        return GroupWallPostComment.objects.filter(
            wall_post=self.get_object()
        )

    def perform_create(self, serializer):
        serializer.save(wall_post=self.get_object(), user=self.request.user)

    def get_object(self):
        if not GroupWallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Group post with this id does not exists.'),
                'create_read_wall_post_comment_group_invalid_post_id'
            )
        obj = GroupWallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj.wall_of_group)
        return obj


class LocationWallCommentAPIView(generics.ListCreateAPIView):
    serializer_class = LocationWallCommentSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return LocationWallPostComment.objects.filter(
            wall_post=self.get_object()
        )

    def perform_create(self, serializer):
        serializer.save(wall_post=self.get_object(), user=self.request.user)

    def get_object(self):
        if not LocationWallPost.objects.filter(id=self.kwargs['post_id']).exists():
            raise exceptions.ValidationError(
                _('Location post with this id does not exists.'),
                'create_read_wall_post_comment_location_invalid_post_id'
            )
        obj = LocationWallPost.objects.get(id=self.kwargs['post_id'])
        self.check_object_permissions(self.request, obj)
        return obj


class UserWallCommentDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WallCommentSerializer
    permission_classes = (IsOwnerOrFriendOfWallPostCommentAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'comment_id'

    def get_object(self):
        if not WallPostComment.objects.filter(id=self.kwargs.get('comment_id')).exists():
            raise exceptions.ValidationError(
                _('Wall post comment with id {} does not exist').format(self.kwargs.get('comment_id')),
                'detail_wall_post_comment_user_invalid_comment_id'
            )

        obj = WallPostComment.objects.get(id=self.kwargs.get('comment_id'))
        self.check_object_permissions(self.request, obj)
        return obj


class GroupWallCommentDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WallCommentSerializer
    permission_classes = (IsOwnerOfWallPostCommentOrMemberOfGroupAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'comment_id'

    def get_object(self):
        if not GroupWallPostComment.objects.filter(id=self.kwargs.get('comment_id')).exists():
            raise exceptions.ValidationError(
                _('Group wall post comment with id {} does not exist').format(self.kwargs.get('comment_id')),
                'detail_wall_post_comment_group_invalid_comment_id'
            )

        obj = GroupWallPostComment.objects.get(id=self.kwargs.get('comment_id'))
        self.check_object_permissions(self.request, obj)
        return obj


class LocationWallCommentDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WallCommentSerializer
    permission_classes = (IsOwnerWallPostCommentOrAdminOfLocationAndAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'comment_id'

    def get_object(self):
        if not LocationWallPostComment.objects.filter(id=self.kwargs.get('comment_id')).exists():
            raise exceptions.ValidationError(
                _('Location wall post comment with id {} does not exist').format(self.kwargs.get('comment_id')),
                'detail_wall_post_comment_location_invalid_comment_id'
            )

        obj = LocationWallPostComment.objects.get(id=self.kwargs.get('comment_id'))
        self.check_object_permissions(self.request, obj)
        return obj
