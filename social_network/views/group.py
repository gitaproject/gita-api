from rest_framework import generics
from rest_framework.exceptions import PermissionDenied

from core_api import permissions
from social_network.models.group import GroupProfile, GroupMembers
from social_network.serializers.group import GroupSerializer, UserGroupSerializer, GroupMemberSerializer, \
    GroupMembersManageSerializer, GroupAdminManageSerializer


class GroupAPIView(generics.ListAPIView):
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    search_fields = ('group_name', 'about')
    queryset = GroupProfile.objects.all().order_by('-group_name')


class GroupCreateAPIView(generics.CreateAPIView):
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    search_fields = ('group_name', 'about')
    queryset = GroupProfile.objects.all().order_by('-group_name')

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class UserGroupPersonalListAPIView(generics.ListAPIView):
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return GroupProfile.objects.joined_groups(self.request.user)


class GroupManageAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'group_id'

    def get_object(self):
        if GroupMembers.objects.filter(
                group=self.kwargs.get('group_id'),
                member=self.request.user,
                admin__isnull=False
        ).exists():
            return GroupProfile.objects.get(id=self.kwargs.get('group_id'))
        else:
            raise PermissionDenied()

    def perform_destroy(self, instance):
        if instance.creator.pk != self.request.user.pk:
            raise PermissionDenied()
        instance.delete()


class GroupDetailAPIView(generics.RetrieveAPIView):
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_field = 'id'
    lookup_url_kwarg = 'group_id'

    def get_object(self):
        return GroupProfile.objects.get(id=self.kwargs.get('group_id'))


class GroupMemberAPIView(generics.ListAPIView):
    serializer_class = GroupMemberSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    search_fields = ('member__first_name', 'member__last_name', 'member__username',)

    def get_queryset(self):
        return GroupMembers.objects.filter(group=self.kwargs['group_id']).order_by('member__username')


class GroupMemberAddAPIView(generics.CreateAPIView):
    serializer_class = GroupMembersManageSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)

    def perform_create(self, serializer):
        if GroupMembers.objects.filter(
                group=self.kwargs.get('group_id'),
                member=self.request.user,
                admin__isnull=False
        ).exists():
            serializer.save(group=GroupProfile.objects.get(pk=self.kwargs.get('group_id')))
        else:
            raise PermissionDenied()


class GroupMemberRemoveAPIView(generics.DestroyAPIView):
    serializer_class = GroupMembersManageSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'group_id'

    def get_object(self):
        return GroupMembers.objects.get(
            group=GroupProfile.objects.get(pk=self.kwargs.get('group_id')),
            member=self.request.data['member']
        )

    def perform_destroy(self, instance):
        if GroupMembers.objects.filter(
                group=self.kwargs.get('group_id'),
                member=self.request.user,
                admin__isnull=False
        ).exists() or instance.member == self.request.user:
            instance.delete()
        else:
            raise PermissionDenied()


class GroupAdminPromoteAPIView(generics.UpdateAPIView):
    serializer_class = GroupAdminManageSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'group_id'

    def get_object(self):
        return GroupMembers.objects.get()

    def perform_update(self, serializer):
        if GroupMembers.objects.filter(
                group=self.kwargs.get('group_id'),
                member=self.request.user,
                admin__isnull=False
        ).exists():
            group_member = GroupMembers.objects.get(id=serializer.validated_data['id'])
            serializer.instance.add_admin(group_member)
        else:
            raise PermissionDenied()


class GroupAdminDemoteAPIView(generics.UpdateAPIView):
    serializer_class = GroupAdminManageSerializer
    permission_classes = (permissions.IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'group_id'

    def get_object(self):
        return GroupMembers.objects.get()

    def perform_update(self, serializer):
        if GroupMembers.objects.filter(
                group=self.kwargs.get('group_id'),
                member=self.request.user,
                admin__isnull=False
        ).exists():
            group_member = GroupMembers.objects.get(id=serializer.validated_data['id'])
            serializer.instance.remove_admin(group_member)
        else:
            raise PermissionDenied()
