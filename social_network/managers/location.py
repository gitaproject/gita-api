from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db import models

from core_api.caches import cache_key

User = get_user_model()


class LocationProfileManager(models.Manager):
    """ LocationProfile manager """

    def administered_locations(self, user: User):
        """ Returns queryset of all locations being administered by the user """
        key = cache_key('locations', user.pk)
        locations = cache.get(key)
        if locations is None:
            from social_network.models import LocationProfile
            locations = LocationProfile.objects.filter(admins__admin=user)
            cache.set(key, locations)
        return locations


class LocationAdminsManager(models.Manager):
    """ LocationAdmins manager """

    def location_admins(self, location):
        """ Return a list of all location admins """
        key = cache_key('location_admins', location.pk)
        location_admins = cache.get(key)
        if location_admins is None:
            location_admins = self.objects.select_related('location').filter(location=location).all().order_by('-id')
            cache.set(key, location_admins)
        return location_admins
