import requests
from django.conf import settings

from social_network.models.sms import SMSHistory, SMSReceivers


def send(content: str, to: list):
    sms_history = SMSHistory.objects.create(content=content)
    SMSReceivers.objects.bulk_create([
        SMSReceivers(phone=t, sms_history=sms_history) for t in to
    ])
    headers = {
        'Accept': 'application/json',
        'Authorization': settings.SMS_GATEWAY_API_KEY,
    }
    data = {
        "content": content,
        "to": [str(t)[1:] for t in to]
    }
    response = requests.post(settings.SMS_GATEWAY_URL, json=data, headers=headers)
    json_response = response.json()
    if json_response.get('messages', False):
        for message in json_response['messages']:
            receiver = sms_history.receivers.all().filter(phone='+' + message['to']).first()
            receiver.api_message_id = message['apiMessageId']
            receiver.accepted = message['accepted']
            receiver.error_code = message['errorCode']
            receiver.error = message['error']
            receiver.error_description = message['errorDescription']
            receiver.save()
    sms_history.error_code = json_response.get('errorCode', None)
    sms_history.error = json_response.get('error', None)
    sms_history.error_description = json_response.get('errorDescription', None)
    sms_history.save()
    return response.status_code == 202
