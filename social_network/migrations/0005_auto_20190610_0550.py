# Generated by Django 2.1.5 on 2019-06-09 22:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import social_network.models.group
import social_network.models.location
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('social_network', '0004_auto_20190606_0253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='groupmembers',
            name='member',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='member_of_groups', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='groupprofile',
            name='creator',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_groups', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='groupprofile',
            name='profile_cover',
            field=models.ImageField(default='images/default_group_cover.jpeg', upload_to=social_network.models.group.group_cover_directory_path),
        ),
        migrations.AlterField(
            model_name='groupprofile',
            name='profile_picture',
            field=sorl.thumbnail.fields.ImageField(default='images/default_group_avatar.jpeg', upload_to=social_network.models.group.group_avatar_directory_path),
        ),
        migrations.AlterField(
            model_name='locationadmins',
            name='admin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='administrated_locations', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='locationprofile',
            name='profile_cover',
            field=models.ImageField(default='images/default_location_cover.jpeg', upload_to=social_network.models.location.location_cover_directory_path),
        ),
        migrations.AlterField(
            model_name='locationprofile',
            name='profile_picture',
            field=sorl.thumbnail.fields.ImageField(default='images/default_location_avatar.jpeg', upload_to=social_network.models.location.location_avatar_directory_path),
        ),
    ]
