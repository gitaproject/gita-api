# Generated by Django 2.1.5 on 2019-01-25 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_network', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gitauser',
            name='first_name',
            field=models.CharField(max_length=30, verbose_name='first name'),
        ),
    ]
