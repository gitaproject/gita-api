from django.urls import path

from social_network.views.location import (
    LocationAPIView, LocationCreateAPIView, UserLocationPersonalListAPIView, LocationManageAPIView,
    LocationDetailAPIView,  # LocationMemberAPIView,
    LocationAdminPromoteAPIView, LocationAdminDemoteAPIView
)

urlpatterns = [
    path('', LocationAPIView.as_view(), name='location_search'),
    path('create/', LocationCreateAPIView.as_view(), name='location_create'),
    path('personal/', UserLocationPersonalListAPIView.as_view(), name='location_personal'),
    path('manage/<int:location_id>/', LocationManageAPIView.as_view(), name='location_manage'),
    path('detail/<int:location_id>/', LocationDetailAPIView.as_view(), name='location_detail'),
    # path('members/<int:location_id>/', LocationMemberAPIView.as_view(), name='location_members'),
    path('promote-admin/<int:location_id>/', LocationAdminPromoteAPIView.as_view(), name='location_promote-admin'),
    path('demote-admin/<int:location_id>/', LocationAdminDemoteAPIView.as_view(), name='location_demote-admin'),
]
