from django.urls import path

from social_network.views.wall import (
    UserWallAPIView, GroupWallAPIView, LocationWallAPIView,

)
from social_network.views.wall_post import (
    UserWallPostDetailAPIView, GroupWallPostDetailAPIView, LocationWallPostDetailAPIView,
)
from social_network.views.wall_comment import (
    UserWallCommentAPIView, GroupWallCommentAPIView, LocationWallCommentAPIView,
    UserWallCommentDetailAPIView, GroupWallCommentDetailAPIView, LocationWallCommentDetailAPIView,
)
from social_network.views.wall_like import (
    UserWallLikeAPIView, GroupWallLikeAPIView, LocationWallLikeAPIView,
)

urlpatterns = [
    path('<int:user_id>/', UserWallAPIView.as_view(), name='wall-user'),
    path('detail/<int:post_id>/', UserWallPostDetailAPIView.as_view(), name='wall-user_detail'),
    path('comment/<int:post_id>/', UserWallCommentAPIView.as_view(), name='wall-user_comment'),
    path('comment/detail/<int:comment_id>/', UserWallCommentDetailAPIView.as_view(),
         name='wall-user_comment_detail'),
    path('like/<int:post_id>/', UserWallLikeAPIView.as_view(), name='wall-user_like'),

    path('group/<int:group_id>/', GroupWallAPIView.as_view(), name='wall-group'),
    path('group/detail/<int:post_id>/', GroupWallPostDetailAPIView.as_view(), name='wall-group_detail'),
    path('group/comment/<int:post_id>/', GroupWallCommentAPIView.as_view(), name='wall-group_comment'),
    path('group/comment/detail/<int:comment_id>/', GroupWallCommentDetailAPIView.as_view(),
         name='wall-group_comment_detail'),
    path('group/like/<int:post_id>/', GroupWallLikeAPIView.as_view(), name='wall-group_like'),

    path('location/<int:location_id>/', LocationWallAPIView.as_view(), name='wall-location'),
    path('location/detail/<int:post_id>/', LocationWallPostDetailAPIView.as_view(), name='wall-location_detail'),
    path('location/comment/<int:post_id>/', LocationWallCommentAPIView.as_view(), name='wall-location_comment'),
    path('location/comment/detail/<int:comment_id>/', LocationWallCommentDetailAPIView.as_view(),
         name='wall-location_comment_detail'),
    path('location/like/<int:post_id>/', LocationWallLikeAPIView.as_view(), name='wall-location_like'),
]
