from django.urls import path, include

urlpatterns = [
    path('feed/', include('social_network.urls.feed'), name='feed'),
    path('profile/', include('social_network.urls.profile'), name='profile'),
    path('group/', include('social_network.urls.group'), name='group'),
    path('location/', include('social_network.urls.location'), name='location'),
    path('wall/', include('social_network.urls.wall'), name='wall'),
    path('explore/', include('social_network.urls.explore'), name='explore'),
    # path('settings/', include('social_network.urls.user_settings'), name='settings'),
]
