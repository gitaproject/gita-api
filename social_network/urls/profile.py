from django.urls import path

from social_network.views.profile import (
    UserProfileAPIView, OtherUserProfileAPIView, UserProfileAvatarAPIView, UserProfileCoverAPIView
)

urlpatterns = [
    path('', UserProfileAPIView.as_view(), name='user_profile-personal'),
    path('avatar/', UserProfileAvatarAPIView.as_view(), name='user_profile-avatar'),
    path('cover/', UserProfileCoverAPIView.as_view(), name='user_profile-cover'),
    path('<str:username>/', OtherUserProfileAPIView.as_view(), name='user_profile-visit'),
]
