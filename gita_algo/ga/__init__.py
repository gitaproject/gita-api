import logging
import sys
from random import *

from gita_algo.utils import best_time
from .fitness import fitness
from .population import population

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


def genetic_algorithm(configuration, data, population_len=20, retain=0.2, random_select=0.37,
                      mutate=0.74, one_result=True, best_posibility=1):
    selected_activity_time = None
    selected_activity_time_score = None
    # logging.debug("===== Start of Genetic Algorithm ======")
    individual_length = configuration['agenda_activity'].duration
    minimum = configuration['agenda_time'][1]
    maximum = configuration['agenda_time'][2]
    # initialize population
    pop = population(population_len, individual_length, minimum, maximum)
    # logging.debug("initial population created: %s", pop)
    max_generation = int((maximum - minimum) / 60)
    # max_generation = max_generation if max_generation < 10000 else 10000
    print()
    print(
        f"Pada titik ini, sebuah siklus algoritma genetika baru akan dimulai.",
        f"Jumlah generasi maksimal dalam siklus ini adalah ({maximum}-{minimum})/{60} dan dibulatkan menjadi {max_generation}.",
        f"Jumlah generasi maksimal tersebut merupakan jumlah kombinasi maksimal untuk rentang waktu yang telah diberikan.",
        f"Dalam siklus ini, individu yang akan terpilih adalah individu yang memiliki score di bawah {maximum - individual_length}.",
        f"Individu yang memiliki score di bawah nilai tersebut merupakan individu yang merepresentasikan sebuah rentang waktu kegiatan yang valid."
        f"Pada awalnya sistem akan membentuk satu populasi yang terdiri dari 20 individu baru yang terpilih secara random:\n"
        f"{pop}"
    )
    # logging.debug("===== Generation Start ======")
    best_time_called = False
    debug_input = False
    for _ in range(max_generation):
        if debug_input:
            debug_input = not bool(input())
        # logging.debug("===== Grading ======")
        # logging.debug("=========== grade individual according their fitness score")
        graded = sorted([(fitness(x, data, maximum - individual_length, minimum), x) for x in pop])
        # logging.debug("population grade: %s", graded)
        # Check if criteria already satisfied
        print(
            f"Kemudian dilakukan proses grading atau perangkingan dimana setiap individu dalam populasi akan diurutkan menurut nilainya yang didapatkan dari fungsi fitness:\n"
            f"Tabel 4.4 Hasil Grading Populasi",
            f"Rangking | Score | Individu\n",
            *[f'{index + 1} | {p[0]} | {p[1]}\n' for index, p in enumerate(graded)]
        )
        # print(
        #     f"Seringkali satu atau lebih individu terburuk memiliki score yang sama, dan dalam kasus ini {maximum - individual_length}.",
        #     f"Dalam siklus ini, nilai tersebut adalah nilai terburuk yang merepresentasikan individu yang tidak valid.",
        #     f"Nilai ini didapatkan karena pada rentang waktu yang dimiliki individu tersebut, host atau lokasi kegiatan tidak tersedia."
        # )
        print(
            f"Setelah diketahui score untuk setiap individu, dilakukan pemeriksaan apakah individu terbaik yang dimiliki sudah berada di bawah ambang batas atas yaitu {maximum - individual_length} atau tidak.",
            f"Jika ditemukan, maka siklus akan berhenti pada generasi ini dan individu terbaik dalam populasi saat ini akan dipilih sebagai retaing waktu terbaik untuk melakukan kegiatan atau aktifitas ini."
        )
        if graded[0][0] <= (maximum - individual_length):
            selected_activity_time = graded[0][1]
            selected_activity_time_score = graded[0][0]
            # print("=======================")
            # print(selected_activity_time_score, selected_activity_time)
            # input()
            # if all([individual[0] == selected_activity_time_score for individual in graded]):
            break

        if best_time_called:
            break
        # logging.debug(
        #     "current best %s with score %s with maximum %s",
        #     selected_activity_time, selected_activity_time_score, (maximum - individual_length)
        # )
        # logging.debug("===== Selection ======")
        # logging.debug("=========== selecting parents for this generation or check if best individual found")
        graded = [x[1] for x in graded]
        retain_length = int(population_len * retain)
        parents = graded[:retain_length]
        # logging.debug("parents selected: %s", parents)
        print(
            f"Jika belum ada individu yang dapat dipilih, maka generasi akan dilanjutkan dengan proses selection dimana individu-individu terbaik akan dipilih sebagai parents.",
            f"Retain length yang digunakan oleh sistem ini adalah 0.2 yang berarti jumlah parents yang akan dipilih pada sistem ini sebanyak 20% dari populasi atau 4 individu.",
            f"Pada generasi ini, parents yang terpilih adalah:\n",
            ', '.join([str(p) for p in parents])
        )

        # logging.debug("===== Mating ======")
        # randomly add other individuals to parents from population to promote genetic diversity
        # logging.debug("=========== mate individuals to produce offspring")
        for individual in graded[retain_length:]:
            if random_select > random():
                parents.append(individual)
        # logging.debug("parents selected: %s", parents)
        print(
            f"Selanjutnya dilakukan proses mating, dimana setiap individu dalam populasi akan memiliki kesempatan "
            f"untuk menjadi parent baru tanpa melihan fitness score individu tersebut.",
            f"Nilai peluang tiap individu untuk terpilih dalam sistem ini sebesar 5%.",
            f"Parents baru yang diberikan oleh perhitungan kali ini setelah proses mating sebagai berikut:\n",
            ', '.join([str(p) for p in parents])
        )

        # logging.debug("===== Mutation ======")
        # logging.debug("=========== promote genetic diversity by mutating some individual")
        print(
            f"Kemudian untuk meningkatkan genetic diversity kromosom-kromosom dalam populasi, dilakukan proses mutation.",
            f"Pada sistem ini setiap parents akan memiliki kesempatan sebesar 1% untuk bermutasi."
        )
        for individual in parents:
            if mutate > random():
                mutation = randint(0, maximum - minimum)
                if individual[0] - mutation <= minimum:
                    mutated_individual = (individual[0] + mutation, individual[1] + mutation)
                else:
                    mutated_individual = (individual[0] - mutation, individual[1] - mutation)
                parents[parents.index(individual)] = mutated_individual
        # logging.debug("parents selected: %s", parents)

        print(
            f"Hasil dari proses mutation oleh sistem sebagai berikut:\n",
            ', '.join([str(p) for p in parents])
        )

        # logging.debug("===== Crossover ======")
        # logging.debug("=========== crossover child with best individuals to get better next generation")
        print(
            f"Kemudian setelah dilakukan reproduksi seleksi, akan dilakukan crossover, dimana dua individu terbaik akan "
            f"diambil bagiannya untuk dibentuk satu individu baru untuk menambah keanekaragaman (diversity) dalam populasi."
        )
        parents_length = len(parents)
        desired_length = population_len - parents_length
        children = []
        while len(children) < desired_length:
            male = randint(0, parents_length - 1)
            female = randint(0, parents_length - 1)
            if male != female:
                male = parents[male]
                # logging.debug("male selected: %s", male)
                female = parents[female]
                # logging.debug("female selected: %s", female)
                if male[0] > female[0]:
                    half = round(abs(male[0] - female[0]) / 2)
                    child = (female[0] + half, female[1] + half)
                else:
                    half = round(abs(female[0] - male[0]) / 2)
                    child = (male[0] + half, male[1] + half)
                children.append(child)
        print(
            f"Hasil crossover yang diberikan oleh sistem sebagai berikut:\n",
            ', '.join([str(p) for p in parents])
        )
        children, best_time_called = best_time(children, individual_length, minimum, data)
        parents.extend(children)
        # logging.debug("Population for next generation: %s", parents)
        print(
            f"Pada tahap ini generasi berakhir dan akan dilanjutkan generasi berikutnya dengan menggunakan populasi baru yaitu:\n"
            f"{pop}"
        )
        pop = parents
    # logging.debug("max_generation: %s", max_generation)
    result = {
        'best_time': selected_activity_time,
        'best_time_score': selected_activity_time_score
    }
    # logging.debug("result: %s", result)
    print(
        f"Dengan demikian, rentang waktu terbaik untuk dilakukan kegiatan ini adalah {selected_activity_time} dengan score {selected_activity_time_score}."
    )
    return result
