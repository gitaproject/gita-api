import logging


def check_schedule(individual, schedules):
    is_schedule_valid = True
    for activity in schedules:
        # logging.debug("activity: %s", activity)
        if activity[0] <= individual[0] <= activity[1] or activity[0] <= individual[1] <= activity[1] \
                or individual[0] <= activity[0] <= individual[1] or individual[0] <= activity[1] <= individual[1]:
            return False
    return is_schedule_valid


def fitness(individual, data, maximum, minimum):
    """
    Determine the fitness of an individual(range time).

    :param maximum:
    :param data: filled time data
    :param individual:range time
    :param args: i will add it later
    :return: fitness value
    """
    # logging.debug("==========================")
    # logging.debug("===== Fitness Check ======")
    # logging.debug("individual selected %s with maximum %s", individual, maximum)
    if check_schedule(individual, data):
        # logging.debug("individual score: %s", individual[0] - minimum)
        return individual[0] - minimum
    # logging.debug("individual score: %s", maximum + 1)
    return maximum + individual[0]
