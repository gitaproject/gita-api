from datetime import datetime, timedelta

import pytz

from gita_algo.loop import Loop
from gita_algo.repeat import Repeat


def parse_time_according_type(schedule, _type, is_range=False):
    the_loop = Loop(schedule)
    if _type[0] == 1:
        # TODO:every second task
        if is_range:
            return schedule
        return schedule
    elif _type[0] == 2:
        # TODO: every minute task
        return schedule
    elif _type[0] == 3:
        # formating hourly task
        if is_range:
            return schedule
        return the_loop.get_hourly()
    elif _type[0] == 4:
        # formating daily task
        if is_range:
            return schedule
        return the_loop.get_daily()
    elif _type[0] == 5:
        # formating weekly task
        return the_loop.get_weekly(is_range)
    elif _type[0] == 6:
        # formating monthly task
        if is_range:
            return schedule
        return the_loop.get_monthly()
    elif _type[0] == 7:
        # formating annualy task
        if is_range:
            return schedule
        return the_loop.get_annualy()
    else:
        return schedule


def reformat_time_according_type(_type, _time):
    """
    (sekali, tiap beberapa detik, menitan, jam2an, harian, mingguan, bulanan, tahunan)
    """
    if _type[0] == 1:
        # TODO:every second task
        return _time
    elif _type[0] == 2:
        # TODO: every minute task
        return _time
    elif _type[0] == 3:
        # formating hourly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_hourly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_hourly()
    elif _type[0] == 4:
        # formating daily task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_daily()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_daily()
    elif _type[0] == 5:
        # formating weekly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_weekly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_weekly()
    elif _type[0] == 6:
        # formating monthly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_monthly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_monthly()
    elif _type[0] == 7:
        # formating annualy task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_annualy()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_annualy()
    else:
        return _time


def datetime_according_type(best_time, _type, _time):
    time_start = _time[1] if isinstance(_time[1], datetime) else datetime.fromtimestamp(_time[1])
    time_until = _time[2] if isinstance(_time[2], datetime) else datetime.fromtimestamp(_time[2])
    best_start = best_time[0] if isinstance(best_time[0], datetime) else datetime.fromtimestamp(
        best_time[0], tz=pytz.utc)
    best_until = best_time[1] if isinstance(best_time[1], datetime) else datetime.fromtimestamp(
        best_time[1], tz=pytz.utc)
    if _type[0] == 1:
        pass
    elif _type[0] == 2:
        pass
    elif _type[0] == 3:
        pass
    elif _type[0] == 4:
        pass
    elif _type[0] == 5:
        first_start = (
                time_start + timedelta(days=(best_start.weekday() - time_start.weekday() + 7) % 7)
        ).replace(
            hour=time_start.hour, minute=time_start.minute, second=time_start.second, microsecond=time_start.microsecond
        )
        first_until = (
                time_until + timedelta(days=(best_until.weekday() - time_until.weekday() + 7) % 7)
        ).replace(
            hour=time_until.hour, minute=time_until.minute, second=time_until.second, microsecond=time_until.microsecond
        )
        if first_start < time_start:
            first_start += timedelta(days=7)
            first_until += timedelta(days=7)
        return [
            [
                first_start + timedelta(days=7 * (i * _type[1])),
                first_until + timedelta(days=7 * (i * _type[1]))
            ] for i in range(_time[0])
        ]
    elif _type[0] == 6:
        pass
    elif _type[0] == 7:
        pass
    else:
        return (best_start, best_until),
    return (best_start, best_until),


def best_time(children, individual_length, minimum, schedule):
    from gita_algo.ga.fitness import check_schedule
    start = minimum
    best_time_called = False
    for activity in schedule:
        new_child = [start, start + individual_length]
        if check_schedule(new_child, schedule):
            children[len(children) - 2] = new_child
            best_time_called = True
        start = activity[1]
    if not best_time_called:
        best_time_called = True
        # print("minimum:", minimum)
        # print("individual_length:", individual_length)
        # print("schedule:")
        # print(schedule)
        # input("wud")
    return children, best_time_called
