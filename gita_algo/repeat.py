from datetime import datetime, timedelta


class Repeat:
    def __init__(self, date_start, interlude, date_until=0, count=0):
        self.date_start = date_start
        self.interlude = interlude
        self.date_until = date_until
        self.count = count

    def getuntil_hourly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(hours=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_hourly(self):
        diff = datetime.fromtimestamp(self.date_start) - datetime.fromtimestamp(self.date_until)
        count = round(diff.seconds / (3600 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_daily(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(days=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_daily(self):
        diff = datetime.fromtimestamp(self.date_start) - datetime.fromtimestamp(self.date_until)
        count = round(diff.days / self.interlude)
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_weekly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(weeks=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_weekly(self):
        diff = datetime.fromtimestamp(self.date_start) - datetime.fromtimestamp(self.date_until)
        count = round(diff.days / (7 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_monthly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(
            days=(self.interlude * self.count * 30))  # relativedelta(months=self.interlude * self.count)
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_monthly(self):
        diff = datetime.fromtimestamp(self.date_start) - datetime.fromtimestamp(self.date_until)
        count = round(diff.days / (30 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_annualy(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(days=(self.interlude * self.count * 365))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_annualy(self):
        diff = datetime.fromtimestamp(self.date_start) - datetime.fromtimestamp(self.date_until)
        count = round(diff.days / (365 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time
