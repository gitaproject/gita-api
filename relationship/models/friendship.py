from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from relationship.managers.friendship import FriendshipManager

User = get_user_model()


class Friendship(models.Model):
    """ Model to represent Friendships """
    to_user = models.ForeignKey(User, related_name='friends', on_delete=models.CASCADE)
    from_user = models.ForeignKey(User, related_name='_unused_friend_relation', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    objects = FriendshipManager()

    class Meta:
        verbose_name = _('Friend')
        verbose_name_plural = _('Friends')
        unique_together = ('from_user', 'to_user')

    def __str__(self):
        return "User #%s is friends with #%s" % (self.to_user_id, self.from_user_id)

    def save(self, *args, **kwargs):
        # Ensure users can't be friends with themselves
        if self.to_user == self.from_user:
            raise ValidationError(_("Users cannot be friends with themselves."))
        super().save(*args, **kwargs)
