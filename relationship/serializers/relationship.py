from django.contrib.auth import get_user_model
from rest_framework import serializers

from relationship.models import Friendship, FriendshipRequest
from social_network.serializers.profile import OtherUserSerializer

User = get_user_model()


class FriendSerializer(serializers.ModelSerializer):
    from_user = OtherUserSerializer(many=False)

    class Meta:
        model = Friendship
        fields = ('id', 'from_user', 'created',)


class FriendRequestSerializer(serializers.ModelSerializer):
    from_user = serializers.StringRelatedField()
    to_user = serializers.StringRelatedField()

    class Meta:
        model = FriendshipRequest
        fields = '__all__'
