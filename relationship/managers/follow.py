from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core_api.caches import cache_key, bust_cache
from core_api.exceptions import AlreadyExistsError
from core_api.signals import (
    leader_created, following_created, follower_created, follower_removed, leader_removed, following_removed
)


class FollowingManager(models.Manager):
    """ Following manager """

    def get_followers(self, user):
        """ Return a list of all followers """
        from relationship.models import Follow
        key = cache_key('followers', user.pk)
        followers = cache.get(key)

        if followers is None:
            queryset = Follow.objects.filter(leader=user).all()
            followers = [follow.follower for follow in queryset]
            cache.set(key, followers)

        return followers

    def get_following(self, user):
        """ Return a list of all users the given user follows """
        from relationship.models import Follow
        key = cache_key('following', user.pk)
        following = cache.get(key)

        if following is None:
            queryset = Follow.objects.filter(follower=user).all()
            following = [follow.leader for follow in queryset]
            cache.set(key, following)

        return following

    def add_follower(self, follower, leader):
        """ Create 'follower' follows 'leader' relationship """
        from relationship.models import Follow
        if follower == leader:
            raise ValidationError(_("Users cannot follow themselves"))

        relation, created = Follow.objects.get_or_create(follower=follower, leader=leader)

        if created is False:
            msg = "User #{} already follows #{}'"
            msg = msg.format(follower, leader)
            raise AlreadyExistsError(msg)

        follower_created.send(sender=self, follower=follower)
        leader_created.send(sender=self, leader=leader)
        following_created.send(sender=self, following=relation)

        bust_cache('followers', leader.pk)
        bust_cache('following', follower.pk)

        return relation

    def remove_follower(self, follower, leader):
        """ Remove 'follower' follows 'leader' relationship """
        from relationship.models import Follow
        if Follow.objects.filter(follower=follower, leader=leader).exists():
            rel = Follow.objects.get(follower=follower, leader=leader)
            follower_removed.send(sender=rel, follower=rel.follower)
            leader_removed.send(sender=rel, leader=rel.leader)
            following_removed.send(sender=rel, following=rel)
            rel.delete()
            bust_cache('followers', leader.pk)
            bust_cache('following', follower.pk)
            return True
        return False

    def check_follows(self, follower, leader):
        """ Does follower follow leader? Smartly uses caches if exists """
        from relationship.models import Follow
        following = cache.get(cache_key('following', follower.pk))
        followers = cache.get(cache_key('followers', leader.pk))

        if followers and follower in followers:
            return True
        elif following and leader in following:
            return True
        else:
            return Follow.objects.filter(follower=follower, leader=leader).exists()
