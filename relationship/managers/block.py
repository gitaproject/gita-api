from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core_api.caches import cache_key, bust_cache
from core_api.exceptions import AlreadyExistsError
from core_api.signals import block_created, block_removed


class BlockManager(models.Manager):
    """ Following manager """

    def get_blocked(self, user):
        """ Return a list of all blocks """
        from relationship.models import Block
        key = cache_key('blocked', user.pk)
        blocked = cache.get(key)

        if blocked is None:
            queryset = Block.objects.filter(blocked=user).all()
            blocked = [block.blocked for block in queryset]
            cache.set(key, blocked)

        return blocked

    def get_blocking(self, user):
        """ Return a list of all users the given user blocks """
        from relationship.models import Block
        key = cache_key('blocking', user.pk)
        blocking = cache.get(key)

        if blocking is None:
            queryset = Block.objects.filter(blocker=user).all()
            blocking = [block.blocked for block in queryset]
            cache.set(key, blocking)

        return blocking

    def add_block(self, blocker, blocked):
        """ Create 'follower' follows 'followee' relationship """
        from relationship.models import Block
        if blocker == blocked:
            raise ValidationError(_("Users cannot block themselves"))

        relation, created = Block.objects.get_or_create(blocker=blocker, blocked=blocked)

        if created is False:
            msg = _("User #{blocked} already blocked by #{blocker}")
            msg = msg.format(blocked=blocked, blocker=blocker)
            raise AlreadyExistsError(msg)
        block_created.send(sender=self, blocker=blocker)
        block_created.send(sender=self, blocked=blocked)
        block_created.send(sender=self, blocking=relation)

        bust_cache('blocked', blocked.pk)
        bust_cache('blocking', blocker.pk)

        return relation

    def remove_block(self, blocker, blocked):
        """ Remove 'blocker' blocks 'blocked' relationship """
        from relationship.models import Block
        if Block.objects.filter(blocker=blocker, blocked=blocked).exists():
            rel = Block.objects.get(blocker=blocker, blocked=blocked)
            block_removed.send(sender=rel, blocker=rel.blocker)
            block_removed.send(sender=rel, blocked=rel.blocked)
            block_removed.send(sender=rel, blocking=rel)
            rel.delete()
            bust_cache('blocked', blocked.pk)
            bust_cache('blocking', blocker.pk)
            return True
        return False

    def is_blocked(self, user1, user2):
        """ Are these two users blocked? """
        from relationship.models import Block
        block1 = cache.get(cache_key('blocks', user1.pk))
        block2 = cache.get(cache_key('blocks', user2.pk))
        if block1 and user2 in block1:
            return True
        elif block2 and user1 in block2:
            return True
        else:
            return Block.objects.filter(blocker=user1, blocked=user2).exists()
