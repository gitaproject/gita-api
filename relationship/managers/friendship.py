from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from core_api.caches import cache_key, bust_cache
from core_api.exceptions import AlreadyFriendsError, AlreadyExistsError, MaximumFriendRequestSentError
from core_api.signals import friendship_request_created, friendship_removed


class FriendshipManager(models.Manager):
    """ Friendship manager """

    def get_friends(self, user):
        """ Return a list of all friends """
        from relationship.models import Friendship
        key = cache_key('friends', user.pk)
        friends = cache.get(key)

        if friends is None:
            queryset = Friendship.objects.select_related('from_user', 'to_user').filter(to_user=user).all()
            friends = queryset
            cache.set(key, friends)

        return friends

    def get_requests(self, user):
        """ Return a list of friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('requests', user.pk)
        requests = cache.get(key)

        if requests is None:
            queryset = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(to_user=user).all()
            requests = list(queryset)
            cache.set(key, requests)

        return requests

    def get_sent_requests(self, user):
        """ Return a list of friendship requests from user """
        from relationship.models import FriendshipRequest
        key = cache_key('sent_requests', user.pk)
        requests = cache.get(key)

        if requests is None:
            qs = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                from_user=user).all()
            requests = list(qs)
            cache.set(key, requests)

        return requests

    def get_unread_requests(self, user):
        """ Return a list of unread friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('unread_requests', user.pk)
        unread_requests = cache.get(key)

        if unread_requests is None:
            qs = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                viewed__isnull=True).all()
            unread_requests = list(qs)
            cache.set(key, unread_requests)

        return unread_requests

    def get_unread_request_count(self, user):
        """ Return a count of unread friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('unread_request_count', user.pk)
        count = cache.get(key)

        if count is None:
            count = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                viewed__isnull=True).count()
            cache.set(key, count)

        return count

    def get_read_requests(self, user):
        """ Return a list of read friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('read_requests', user.pk)
        read_requests = cache.get(key)

        if read_requests is None:
            qs = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                viewed__isnull=False).all()
            read_requests = list(qs)
            cache.set(key, read_requests)

        return read_requests

    def get_rejected_requests(self, user):
        """ Return a list of rejected friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('rejected_requests', user.pk)
        rejected_requests = cache.get(key)

        if rejected_requests is None:
            qs = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                rejected__isnull=False).all()
            rejected_requests = list(qs)
            cache.set(key, rejected_requests)

        return rejected_requests

    def get_unrejected_requests(self, user):
        """ All requests that haven't been rejected """
        from relationship.models import FriendshipRequest
        key = cache_key('unrejected_requests', user.pk)
        unrejected_requests = cache.get(key)

        if unrejected_requests is None:
            qs = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                rejected__isnull=True).all()
            unrejected_requests = list(qs)
            cache.set(key, unrejected_requests)

        return unrejected_requests

    def get_unrejected_request_count(self, user):
        """ Return a count of unrejected friendship requests """
        from relationship.models import FriendshipRequest
        key = cache_key('unrejected_request_count', user.pk)
        count = cache.get(key)

        if count is None:
            count = FriendshipRequest.objects.select_related('from_user', 'to_user').filter(
                to_user=user,
                rejected__isnull=True).count()
            cache.set(key, count)

        return count

    def add_friend(self, from_user, to_user, message=None):
        """ Create a friendship request """
        from relationship.models import FriendshipRequest
        if from_user == to_user:
            raise ValidationError(_("Users cannot be friends with themselves"))

        if self.are_friends(from_user, to_user):
            raise AlreadyFriendsError(_("Users are already friends"))

        if not self.can_request_send(from_user, to_user):
            raise AlreadyExistsError(_("Friendship already requested"))

        if FriendshipRequest.objects.filter(
                from_user=from_user,
                to_user=to_user,
                rejected__isnull=False,
                retry_count__lte=settings.MAXIMUM_FRIEND_REQUEST_COUNT_SENT
        ).exists():
            raise MaximumFriendRequestSentError(_("Friend request sen"))

        if message is None:
            message = ''

        friend_request, created = FriendshipRequest.objects.get_or_create(
            from_user=from_user,
            to_user=to_user,
        )

        if not created:
            friend_request.rejected = None
            friend_request.viewed = None
            friend_request.retry_count += 1

        if message:
            friend_request.message = message

        if not created or message:
            friend_request.save()

        bust_cache('requests', to_user.pk)
        bust_cache('sent_requests', from_user.pk)
        friendship_request_created.send(sender=friend_request)

        return friend_request

    def can_request_send(self, from_user, to_user):
        """ Checks if a friend request can be done """
        from relationship.models import FriendshipRequest
        return from_user != to_user and not FriendshipRequest.objects.filter(
            Q(from_user=from_user, to_user=to_user),
            Q(rejected__isnull=True) | Q(retry_count__gt=settings.MAXIMUM_FRIEND_REQUEST_COUNT_SENT)
        ).exists() and not self.are_friends(from_user, to_user)

    def remove_friend(self, from_user, to_user):
        """ Destroy a friendship relationship """
        from relationship.models import Friendship
        queryset = Friendship.objects.filter(
            Q(to_user=to_user, from_user=from_user) |
            Q(to_user=from_user, from_user=to_user)
        ).all()

        if queryset:
            friendship_removed.send(
                sender=queryset[0],
                from_user=from_user,
                to_user=to_user
            )
            queryset.delete()
            bust_cache('friends', to_user.pk)
            bust_cache('friends', from_user.pk)
            return True
        else:
            return False

    def are_friends(self, user1, user2):
        """ Are these two users friends? """
        from relationship.models import Friendship
        friends1 = cache.get(cache_key('friends', user1.pk))
        friends2 = cache.get(cache_key('friends', user2.pk))
        if friends1 and user2 in friends1:
            return True
        elif friends2 and user1 in friends2:
            return True
        else:
            return Friendship.objects.filter(to_user=user1, from_user=user2).exists()
