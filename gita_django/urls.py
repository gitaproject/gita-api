"""
    gita_django URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Gita API",
        default_version=settings.API_VERSION,
        description="""
        
        This is documentation of Gita open API
        
        """,
        terms_of_service="https://inchidi.id/gita/tos/",
        contact=openapi.Contact(email="dimasinchidi@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = \
    [
        path('admin/', admin.site.urls),
        path('api/' + settings.API_VERSION + '/', include('core_api.urls'), name='api'),
    ] + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    ) + static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )

if settings.DEBUG:
    urlpatterns += [
        path('', schema_view.with_ui('redoc', cache_timeout=None), name='schema-redoc'),
        re_path(r'^swagger(?P<format>.json|.yaml)$', schema_view.without_ui(cache_timeout=None),
                name='schema-json'),
        path('swagger/', schema_view.with_ui('swagger', cache_timeout=None), name='schema-swagger-ui'),
    ]
