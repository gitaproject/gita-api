from django.apps import AppConfig


class UnitTestsConfig(AppConfig):
    name = 'unit_tests'
