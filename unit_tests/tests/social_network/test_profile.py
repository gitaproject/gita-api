import os

from django.conf import settings
from django.urls import reverse
from rest_framework import status

from social_network.models import GroupMembers, LocationAdmins
from unit_tests.tests.common import PreparedTestCase


class ProfileTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_add_profile_picture(self):
        file_path = os.path.join(settings.BASE_DIR, 'root_media/test_profile_picture.jpg')
        with open(file_path, 'rb') as file:
            response = self.client_users[0].put(
                reverse('user_profile-avatar'),
                {'avatar': file},
                format='multipart'
            )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertIn('test_profile_picture', response.data['avatar'])

    def test_add_profile_cover_picture(self):
        file_path = os.path.join(settings.BASE_DIR, 'root_media/test_cover_picture.gif')
        with open(file_path, 'rb') as file:
            response = self.client_users[0].put(
                reverse('user_profile-cover'),
                {'cover': file},
                format='multipart'
            )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertIn('test_cover_picture', response.data['cover'])

    def test_read_user_profile(self):
        response = self.client_users[0].get(reverse('user_profile-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['username'], self.object_users[0].username, response.data)
        self.assertEqual(response.data['first_name'], self.object_users[0].first_name, response.data)
        self.assertEqual(response.data['last_name'], self.object_users[0].last_name, response.data)
        self.assertEqual(response.data['email'], self.object_users[0].email, response.data)
        self.assertEqual(response.data['phone'], self.object_users[0].phone, response.data)
        self.assertEqual(response.data['profile']['gender'], self.object_users[0].profile.gender, response.data)

    def test_read_other_user_profile(self):
        response = self.client_users[0].get(
            reverse('user_profile-visit', kwargs={'username': self.data_users[1]['username']})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['username'], self.object_users[1].username, response.data)
        self.assertEqual(response.data['first_name'], self.object_users[1].first_name, response.data)
        self.assertEqual(response.data['last_name'], self.object_users[1].last_name, response.data)
        self.assertEqual(response.data['profile']['gender'], self.object_users[1].profile.gender, response.data)

    def test_read_friends_profile(self):
        response = self.client_users[0].get(self.reversed_urls['friendship_personal'])
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_read_group_profile(self):
        response = self.client_users[0].get(
            reverse('group_detail', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['group_name'], self.object_groups[0].group_name, response.data)
        self.assertEqual(response.data['about'], self.object_groups[0].about, response.data)

    def test_read_joined_group_profile(self):
        response = self.client_users[0].get(reverse('group_personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        for group in response.data['results']:
            self.assertTrue(
                GroupMembers.objects.filter(
                    group__id=group['id'],
                    member=self.object_users[0]
                ).exists()
            )

    def test_read_location_profile(self):
        response = self.client_users[0].get(
            reverse('location_detail', kwargs={'location_id': self.object_locations[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['location_name'], self.object_locations[0].location_name, response.data)
        self.assertEqual(response.data['about'], self.object_locations[0].about, response.data)

    def test_read_managed_location_profile(self):
        response = self.client_users[0].get(reverse('location_personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        for location in response.data['results']:
            self.assertTrue(
                LocationAdmins.objects.filter(
                    location__id=location['id'],
                    admin=self.object_users[0]
                ).exists()
            )
