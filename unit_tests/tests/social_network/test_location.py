from django.urls import reverse
from rest_framework import status

from social_network.models import LocationProfile
from unit_tests.tests.common import PreparedTestCase


class LocationTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_create_location(self):
        location_data = {
            'location_name': 'New Location',
            'about': 'testing new location',
        }
        response = self.client_users[0].post(reverse('location_create'), location_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['location_name'],
            location_data['location_name']
        )
        self.assertEqual(
            LocationProfile.objects.all().count(),
            len(self.data_locations) + 1
        )

    def test_set_location_time(self):
        pass

    def test_update_location(self):
        pass
        # new_group_data = {
        #     'group_name': 'Updated Group',
        #     'about': 'testing update group',
        # }
        # # user not admin of the group
        # response = self.client_users[5].patch(
        #     reverse('group_manage', kwargs={'group_id': self.object_groups[0].id}),
        #     new_group_data
        # )
        # self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        # self.assertNotEqual(
        #     new_group_data['group_name'],
        #     GroupProfile.objects.get(id=self.object_groups[0].id).group_name
        # )
        # # user is admin of the group
        # response = self.client_users[0].patch(
        #     reverse('group_manage', kwargs={'group_id': self.object_groups[0].id}),
        #     new_group_data
        # )
        # self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        # self.assertEqual(
        #     new_group_data['group_name'],
        #     GroupProfile.objects.get(id=self.object_groups[0].id).group_name
        # )

    def test_retrieve_location(self):
        response = self.client_users[5].get(
            reverse('location_detail', kwargs={'location_id': self.object_locations[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['location_name'],
            self.object_locations[0].location_name
        )

    def test_delete_location(self):
        # admin of the group could not delete
        response = self.client_users[1].delete(
            reverse('location_manage', kwargs={'location_id': self.object_locations[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        # creator of the group could delete
        response = self.client_users[0].delete(
            reverse('location_manage', kwargs={'location_id': self.object_locations[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
