from django.urls import reverse
from rest_framework import status

from unit_tests.tests.common import PreparedTestCase


class RelationshipTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_add_friend(self):
        to_user_index = 6
        from_user_index = 3
        add_friend_data = {
            'username': self.data_users[to_user_index]['username'],
        }
        response = self.client_users[from_user_index].post(reverse('friendship-add'), add_friend_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(response.data['messages'], 'Friend request sent.', response.data)
        return to_user_index, from_user_index

    def test_cancel_friend_request(self):
        to_user_index, from_user_index = self.test_add_friend()
        cancel_friend_data = {
            'username': self.data_users[to_user_index]['username'],
        }
        response = self.client_users[from_user_index].delete(reverse('friendship-cancel_request'), cancel_friend_data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_read_friend_requests(self):
        to_user_index, from_user_index = self.test_add_friend()
        response = self.client_users[to_user_index].get(reverse('friendship-requests'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['results'][0]['from_user']['username'],
            self.data_users[from_user_index]['username'],
            response.data
        )

    def test_accept_friend_request(self):
        to_user_index, from_user_index = self.test_add_friend()
        accept_friend_data = {
            'username': self.data_users[from_user_index]['username'],
        }
        response = self.client_users[to_user_index].post(reverse('friendship-accept_request'), accept_friend_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        return to_user_index, from_user_index

    def test_reject_friend_request(self):
        to_user_index, from_user_index = self.test_add_friend()
        reject_friend_data = {
            'username': self.data_users[from_user_index]['username'],
        }
        response = self.client_users[to_user_index].post(reverse('friendship-reject_request'), reject_friend_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_remove_friend(self):
        to_user_index, from_user_index = self.test_accept_friend_request()
        remove_friend_data = {
            'username': self.data_users[to_user_index]['username'],
        }
        response = self.client_users[from_user_index].delete(reverse('friendship-remove'), remove_friend_data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

        response = self.client_users[from_user_index].get(reverse('friendship-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(len(response.data['results']), 0)

    def test_read_friends(self):
        to_user_index, from_user_index = self.test_accept_friend_request()

        response = self.client_users[to_user_index].get(reverse('friendship-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['results'][0]['from_user']['username'],
            self.data_users[from_user_index]['username'],
            response.data
        )

        response = self.client_users[from_user_index].get(reverse('friendship-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['results'][0]['from_user']['username'],
            self.data_users[to_user_index]['username'],
            response.data
        )
