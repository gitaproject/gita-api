import copy
from datetime import timedelta, datetime
from random import choice

import pytz
from django.conf import settings
from django.db import connections
from django.test import override_settings
from django.urls import reverse
from django.utils.timezone import now
from rest_framework.test import APITestCase, APIClient

from social_network.models import (
    GitaUser, GroupProfile, LocationProfile, WallPostLikes, WallPost, WallPostComment, GroupWallPost,
    GroupWallPostLikes, GroupWallPostComment, Posts, UserProfile
)
from relationship.models import (
    Friendship, FriendshipRequest, Follow, Block
)
from notification.models import Notification
from agenda.models import (
    Agenda, Activities, AgendaMember, AgendaLocationCandidate, AgendaHost, AgendaActivity,
    InvitationActivities, UserActivities
)


@override_settings(PASSWORD_HASHERS=['django.contrib.auth.hashers.MD5PasswordHasher'])
class PreparedTestCase(APITestCase):
    tz = pytz.timezone(settings.TIME_ZONE)
    object_admins = []
    object_users = []
    object_groups = []
    object_locations = []
    object_agendas = []
    object_activities = []
    object_user_activities = []
    object_user_wall_posts = []
    object_group_wall_posts = []
    object_friends = []
    object_posts = []
    object_notifications = []
    client_admins = []
    client_users = []
    reversed_urls = {
        'login_user': reverse('authentication-login'),
        'logout_user': reverse('authentication-logout'),

        'registration_check_username': reverse('registration-check_username'),
        'registration_register_user': reverse('registration-register'),
        'registration_verify_token_resend': reverse('registration-resend_token'),
        'registration_verify_phone': reverse('registration-verify_phone'),

        # 'group_list_create': reverse('group_list_create'),
        'feed': reverse('feed'),

        'friendship_personal': reverse('friendship-personal'),
    }

    @classmethod
    def tearDownClass(cls):
        # close connection but do not delete data before all test end
        if not cls.allow_database_queries:
            for alias in connections:
                connection = connections[alias]
                connection.cursor = connection.cursor.wrapped
                connection.chunked_cursor = connection.chunked_cursor.wrapped
        if hasattr(cls, '_cls_modified_context'):
            cls._cls_modified_context.disable()
            delattr(cls, '_cls_modified_context')
        if hasattr(cls, '_cls_overridden_context'):
            cls._cls_overridden_context.disable()
            delattr(cls, '_cls_overridden_context')

    @classmethod
    def setUpTestData(cls):
        settings.IS_ONLINE = False
        cls.data_admins = (
            {
                'username': 'administrator',
                'email': 'administrator@test.com',
                'password': '3xtr4s7r0n9p455',
                'first_name': 'John',
                'last_name': 'Doe',
                'phone': '+628000000001',
            },
        )
        cls.prepare_admins(cls.data_admins)
        cls.data_users = (
            {
                'username': 'dimasinchidi',
                'email': 'dimasinchidi@test.com',
                'password': 's7r0n9p455',
                'first_name': 'Dimas',
                'last_name': 'Ari',
                'phone': '+628152121216',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test1',
                'email': 'user_test1@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Satu',
                'phone': '+628152121201',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test2',
                'email': 'user_test2@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Dua',
                'phone': '+628152121202',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test3',
                'email': 'user_test3@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Tiga',
                'phone': '+628152121203',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test4',
                'email': 'user_test4@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Empat',
                'phone': '+628152121204',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test5',
                'email': 'user_test5@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Lima',
                'phone': '+628152121205',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test6',
                'email': 'user_test6@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Enam',
                'phone': '+628152121206',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
            {
                'username': 'user_test7',
                'email': 'user_test7@test.com',
                'password': 's7r0n9p455',
                'last_name': 'Test',
                'first_name': 'Tujuh',
                'phone': '+628152121207',
                'profile': {
                    'birthday': '{}-{}-{}'.format(
                        choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                    'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
                }
            },
        )
        cls.prepare_users(cls.data_users)
        cls.data_groups = (
            {
                'username': 'first_group',
                'group_name': 'First Sample Group',
                'about': 'This is first dummy group',
                'creator': cls.object_users[0],
                'admins': (cls.object_users[1], cls.object_users[2]),
                'members': (
                    cls.object_users[3], cls.object_users[4], cls.object_users[5],
                ),
            },
            {
                'username': 'second_group',
                'group_name': 'Second Sample Group',
                'about': 'This is second dummy group',
                'creator': cls.object_users[0],
                'admins': (cls.object_users[1], cls.object_users[2]),
                'members': (
                    cls.object_users[3], cls.object_users[4], cls.object_users[5],
                    cls.object_users[6], cls.object_users[7],
                ),
            },
            {
                'username': 'someone_else_group',
                'group_name': 'Someone Else Sample Group',
                'about': 'This is someone else dummy group',
                'creator': cls.object_users[5],
                'admins': (cls.object_users[6], cls.object_users[7]),
                'members': (
                    cls.object_users[0], cls.object_users[1], cls.object_users[2], cls.object_users[3],
                    cls.object_users[4],
                ),
            },
        )
        cls.prepare_groups(cls.data_groups)
        cls.data_locations = (
            {
                'username': 'first_location',
                'location_name': 'First Sample Location',
                'about': 'This is first dummy group',
                'creator': cls.object_users[0],
                'admins': (cls.object_users[1], cls.object_users[2]),
            },
            {
                'username': 'second_location',
                'location_name': 'Second Sample Location',
                'about': 'This is second dummy group',
                'creator': cls.object_users[0],
                'admins': (cls.object_users[1], cls.object_users[2]),
            },
            {
                'username': 'someone_else_location',
                'location_name': 'Someone Else Sample Location',
                'about': 'This is someone else dummy group',
                'creator': cls.object_users[5],
                'admins': (cls.object_users[6], cls.object_users[7]),
            },
        )
        cls.prepare_locations(cls.data_locations)
        cls.tomorrow = datetime.now() + timedelta(days=1)
        cls.yesterday = datetime.now() - timedelta(days=1)
        cls.data_agendas = (
            # User Individual One-Time Agenda Activity
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'User Individual One-Time Agenda',
                'description': 'User personal agenda that will '
                               'only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                # 'loop_count': 0,  # not required since range_time_until already provided
                'range_time_start': '{}-{}-{}T8:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'range_time_until': '{}-{}-{}T15:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Berenang',
                        'description': 'Segerin diri, berenang dulu',
                        'duration': 3600,  # one hour
                    },
                ),
            },
            # User Individual Repeat Agenda Activity
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'User Individual Repeat Agenda',
                'description': 'User personal agenda that will '
                               'generated multiple times in multiple time range',
                'loop': Agenda.DAILY,
                'loop_interlude': 1,  # equals with everyday
                'loop_count': 30,  # equals with repeat for a month
                'range_time_start': '{}-{}-{}T00:00:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                # range_time_until not required if loop_count provided
                # 'range_time_until': '{}-{}-{}T00:00:00+07:00'.format(
                #     cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=30)).day
                # ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Mandi',
                        'description': 'Bersihin diri, mandi dulu',
                        'duration': 60 * 10,  # 10 minutes
                    },
                ),
            },
            # Users (Party) One-Time Agenda Activity
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'Users (Party) One-Time Agenda',
                'description': 'User party (with it friend/s by tagging them) agenda '
                               'that will only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Lari Pagi',
                        'description': 'Segerin badan, lari pagi bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                ),
            },
            # Users (Party) Repeat Agenda Activity
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'Users (Party) Repeat Agenda',
                'description': 'User party (with it friend/s by tagging them) agenda '
                               'that will generated multiple times in multiple time range',
                'loop': Agenda.WEEKLY,
                'loop_interlude': 2,  # equals with every two week
                # 'loop_count': 4,  # equals with in between 90 days, do it 4 times
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=90)).day
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Badminton',
                        'description': 'Latih respon, badminton bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                ),
            },
            # Group One-Time Agenda Activity
            {
                'creator': cls.object_users[0],
                'group': cls.object_groups[0],
                'agenda_name': 'Group One-Time Agenda',
                'description': 'Group agenda that will only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                'range_time_start': '{}-{}-{}T09:00:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T16:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Rapat',
                        'description': 'Rapat mendadak, ngumpul dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                ),
            },
            # Group Repeat Agenda Activity
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'Group Repeat Agenda',
                'description': 'Group agenda that will generated multiple times in multiple time range',
                'loop': Agenda.WEEKLY,
                'loop_interlude': 2,  # equals with every two week
                'loop_count': 4,  # equals with in between 90 days, do it 4 times
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                # 'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                #     cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=90)).day
                # ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Badminton',
                        'description': 'Latih respon, badminton bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                ),
            },
            # User Individual One-Time Agenda Activities
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'User Individual One-Time Agenda with Activities',
                'description': 'User personal agenda with multiple activities that will '
                               'only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                # 'loop_count': 0,  # not required since range_time_until already provided
                'range_time_start': '{}-{}-{}T8:00:00+07:00'.format(cls.tomorrow.year, str(cls.tomorrow.month).zfill(2),
                                                                    str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T15:00:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Berenang',
                        'description': 'Segerin diri, berenang dulu',
                        'duration': 3600,
                    },
                    {
                        'activity_name': 'Workout',
                        'description': 'Bugarin diri, workout dulu',
                        'duration': 3600,
                    },
                ),
            },
            # User Individual Repeat Agenda Activities
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'User Individual Repeat Agenda with Activities',
                'description': 'User personal agenda with multiple activities that will '
                               'generated multiple times in multiple time range',
                'loop': Agenda.DAILY,
                'loop_interlude': 1,  # equals with everyday
                'loop_count': 30,  # equals with repeat for a month
                'range_time_start': '{}-{}-{}T00:00:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                # range_time_until not required if loop_count provided
                # 'range_time_until': '{}-{}-{}T00:00:00+07:00'.format(
                #     cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=30)).day
                # ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Mandi',
                        'description': 'Bersihin diri, mandi dulu',
                        'duration': 60 * 10,
                    },
                    {
                        'activity_name': 'Makan',
                        'description': 'Kenyangin diri, makan dulu',
                        'duration': 60 * 10,
                    },
                ),
            },
            # Users (Party) One-Time Agenda Activities
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'Users (Party) One-Time Agenda with Activities',
                'description': 'User party (with it friend/s by tagging them) agenda with multiple activities '
                               'that will only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Lari',
                        'description': 'Segerin badan, lari bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                    {
                        'activity_name': 'Festival',
                        'description': 'Cuci mata, ke festival bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                ),
            },
            # Users (Party) Repeat Activities
            {
                'creator': cls.object_users[0],
                'user': cls.object_users[0],
                'agenda_name': 'Users (Party) Repeat Agenda with Activities',
                'description': 'User party (with it friend/s by tagging them) agenda with multiple activities '
                               'that will generated multiple times in multiple time range',
                'loop': Agenda.WEEKLY,
                'loop_interlude': 2,  # equals with every two week
                # 'loop_count': 4,  # equals with in between 90 days, do it 4 times
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=90)).day
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Belajar Bersama',
                        'description': 'Ulangan udah deket, belajar bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                    {
                        'activity_name': 'Gym',
                        'description': 'Kurusin badan, paketan gym bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                        'agenda_members': (
                            cls.object_users[1], cls.object_users[2], cls.object_users[3], cls.object_users[4],
                            cls.object_users[5], cls.object_users[6], cls.object_users[7],
                        )
                    },
                ),
            },
            # Group (Party) One-Time Activities
            {
                'creator': cls.object_users[0],
                'group': cls.object_groups[0],
                'agenda_name': 'Group One-Time Agenda with Activities',
                'description': 'Group agenda with multiple activities '
                               'that will only generated once or not repeat in specific range time',
                'loop': Agenda.NO_LOOP,
                'range_time_start': '{}-{}-{}T09:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'range_time_until': '{}-{}-{}T16:00:00+07:00'.format(
                    cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), str(cls.tomorrow.day).zfill(2)
                ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Seminar A',
                        'description': 'Ada seminar a, ikut seminar bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                    {
                        'activity_name': 'Seminar B',
                        'description': 'Ada seminar b, ikut seminar bareng dulu',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                ),
            },
            # Group (Party) Repeat Activities
            {
                'creator': cls.object_users[0],
                'group': cls.object_groups[0],
                'agenda_name': 'Group Repeat Agenda with Activities',
                'description': 'Group agenda with multiple activities '
                               'that will generated multiple times in multiple time range',
                'loop': Agenda.WEEKLY,
                'loop_interlude': 2,  # equals with every two week
                'loop_count': 4,  # equals with in between 90 days, do it 4 times
                'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(cls.tomorrow.year,
                                                                     str(cls.tomorrow.month).zfill(2),
                                                                     str(cls.tomorrow.day).zfill(2)),
                # 'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                #     cls.tomorrow.year, str(cls.tomorrow.month).zfill(2), (cls.tomorrow + timedelta(days=90)).day
                # ),
                'agenda_location_candidates': (
                    cls.object_locations[0], cls.object_locations[1],
                ),
                'agenda_activities': (
                    {
                        'activity_name': 'Kelas A',
                        'description': 'Ada kelas a, jangan telat.',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                    {
                        'activity_name': 'Kelas B',
                        'description': 'Ada kelas b, jangan telat.',
                        'duration': 3600,
                        'agenda_hosts': (cls.object_users[0],),
                    },
                ),
            },
        )
        # cls.prepare_agendas(cls.data_agendas)
        cls.data_activities = (
            {
                # User Minimal Exact Activity
                'time_start': '{}-{}-{}T18:00:00+07:00'.format(cls.tomorrow.year, str(cls.tomorrow.month).zfill(2),
                                                               str(cls.tomorrow.day).zfill(2)),
                'time_until': '{}-{}-{}T19:00:00+07:00'.format(cls.tomorrow.year, str(cls.tomorrow.month).zfill(2),
                                                               str(cls.tomorrow.day).zfill(2)),
                'activity_name': 'Eat alone cls.tomorrow',
                'description': 'User individual or personal activity with specific time (could be past time) '
                               'which not generated by the algorithm.',
                'user_activity': (cls.object_users[0],),
            },
        )
        # cls.prepare_activities(cls.data_activities)
        cls.data_user_wall_posts = (
            {
                'user': cls.object_users[0],
                'wall_of_user': cls.object_users[0],
                'body': '''My first post!''',
                'created_at': '{}-{}-{}T13:00:00+07:00'.format(cls.yesterday.year, cls.yesterday.month,
                                                               cls.yesterday.day),
                'likes': (
                    {
                        'user': cls.object_users[0],
                    },
                ),
                'comments': (
                    {
                        'user': cls.object_users[0],
                        'comment': 'This is comment',
                    },
                )
            },
        )
        cls.prepare_user_wall_posts(cls.data_user_wall_posts)
        cls.data_group_wall_posts = tuple()
        cls.prepare_group_wall_posts(cls.data_group_wall_posts)
        cls.data_friends = (
            {
                'to_user': cls.object_users[0],
                'from_user': cls.object_users[1],
            },
        )
        cls.prepare_friends(cls.data_friends)

    @classmethod
    def prepare_admins(cls, data_admins):
        for data_admin in data_admins:
            if not GitaUser.objects.filter(phone=data_admin['phone']).exists():
                data_admin_temp = copy.deepcopy(data_admin)
                phone = data_admin_temp.pop('phone')
                email = data_admin_temp.pop('email')
                password = data_admin_temp.pop('password')
                data_admin_temp['email_verified'] = now()
                data_admin_temp['phone_verified'] = now()
                user = GitaUser.objects.create_superuser(
                    phone, email, password, **data_admin_temp
                )
                cls.object_admins.append(user)
                cls.client_admins.append(APIClient(enforce_csrf_checks=True))
        assert len(cls.client_admins) == len(data_admins)

    @classmethod
    def prepare_users(cls, data_users):
        for data_user in data_users:
            if not GitaUser.objects.filter(phone=data_user['phone']).exists():
                data_user_temp = copy.deepcopy(data_user)
                data_profile = data_user_temp.pop('profile')
                phone = data_user_temp.pop('phone')
                password = data_user_temp.pop('password')
                email = data_user_temp.pop('email')
                data_user_temp['email_verified'] = now()
                data_user_temp['phone_verified'] = now()
                user = GitaUser.objects.create_gita_user(
                    phone, data_profile, password, email, **data_user_temp
                )
                cls.object_users.append(user)
                cls.client_users.append(APIClient(enforce_csrf_checks=True))
        assert len(cls.client_users) == len(data_users)
        cls.client_anonymous = APIClient(enforce_csrf_checks=True)

    @classmethod
    def prepare_groups(cls, data_groups):
        for data_group in data_groups:
            if not GroupProfile.objects.filter(group_name=data_group['group_name']).exists():
                admins = data_group.pop('admins')
                members = data_group.pop('members')
                group = GroupProfile.objects.create(**data_group)
                cls.object_groups.append(group)
                for admin in admins:
                    group.add_admin(admin)
                for member in members:
                    group.add_member(member)
        # print('total group', len(cls.object_groups))
        assert len(cls.object_groups) == len(data_groups)

    @classmethod
    def prepare_locations(cls, data_locations):
        for data_location in data_locations:
            if not LocationProfile.objects.filter(location_name=data_location['location_name']).exists():
                admins = data_location.pop('admins')
                location = LocationProfile.objects.create(**data_location)
                cls.object_locations.append(location)
                for admin in admins:
                    location.add_admin(admin)
        # print('total location', len(cls.object_locations))
        assert len(cls.object_locations) == len(data_locations)

    @classmethod
    def prepare_agendas(cls, data_agendas):
        for data_agenda in data_agendas:
            if not Agenda.objects.filter(agenda_name=data_agenda['agenda_name']).exists():
                data_agenda_location_candidates = data_agenda.pop('agenda_location_candidates')
                data_agenda_activities = data_agenda.pop('agenda_activities')
                agenda = Agenda.objects.create(**data_agenda)
                for data_agenda_location_candidate in data_agenda_location_candidates:
                    AgendaLocationCandidate.objects.create(agenda=agenda, location=data_agenda_location_candidate)
                for data_agenda_activity in data_agenda_activities:
                    data_agenda_activity_hosts = data_agenda_activity.pop(
                        'agenda_hosts') if data_agenda_activity.get(
                        'agenda_hosts', False) else tuple()
                    data_agenda_activity_members = data_agenda_activity.pop(
                        'agenda_members') if data_agenda_activity.get(
                        'agenda_members', False) else tuple()
                    agenda_activity = AgendaActivity.objects.create(agenda=agenda, **data_agenda_activity)
                    for data_agenda_activity_host in data_agenda_activity_hosts:
                        AgendaHost.objects.create(agenda_activity=agenda_activity, host=data_agenda_activity_host)
                    for data_agenda_activity_member in data_agenda_activity_members:
                        AgendaMember.objects.create(agenda_activity=agenda_activity, member=data_agenda_activity_member)
                cls.object_agendas.append(agenda)
            # else:
            #     cls.object_agendas.append(Agenda.objects.get(agenda_name=data_agenda['agenda_name']))

        # print('total agenda', len(cls.object_agendas))
        assert len(cls.object_agendas) == len(data_agendas)

    @classmethod
    def prepare_activities(cls, data_activities):
        for data_activity in data_activities:
            if not Activities.objects.filter(activity_name=data_activity['activity_name']).exists():
                users = data_activity.pop('user_activity')
                activity = Activities.objects.create(**data_activity)
                for user in users:
                    user_activities = UserActivities.objects.create(user=user, activity=activity)
                    cls.object_user_activities.append(user_activities)
                cls.object_activities.append(activity)

        # print('total activity', len(cls.object_activities))
        assert len(cls.object_activities) == len(data_activities)

    @classmethod
    def prepare_user_wall_posts(cls, data_user_wall_posts):
        for data_user_wall_post in data_user_wall_posts:
            if not WallPost.objects.filter(
                    wall_of_user=data_user_wall_post['wall_of_user'],
                    body=data_user_wall_post['body']
            ).exists():
                likes = data_user_wall_post.pop('likes')
                comments = data_user_wall_post.pop('comments')
                wall_post = WallPost.objects.create(**data_user_wall_post)
                for like in likes:
                    WallPostLikes.objects.create(wall_post=wall_post, **like)
                for comment in comments:
                    WallPostComment.objects.create(wall_post=wall_post, **comment)
                cls.object_user_wall_posts.append(wall_post)

        # print('total user wall post', len(cls.object_user_wall_posts))
        assert len(cls.object_user_wall_posts) == len(data_user_wall_posts)

    @classmethod
    def prepare_group_wall_posts(cls, data_group_wall_posts):
        for data_group_wall_post in data_group_wall_posts:
            if not GroupWallPost.objects.filter(
                    wall_of_group=data_group_wall_post['wall_of_group'],
                    body=data_group_wall_post['body']
            ).exists():
                likes = data_group_wall_post.pop('likes')
                comments = data_group_wall_post.pop('comments')
                wall_post = GroupWallPost.objects.create(**data_group_wall_post)
                for like in likes:
                    GroupWallPostLikes.objects.create(wall_post=wall_post, **like)
                for comment in comments:
                    GroupWallPostComment.objects.create(wall_post=wall_post, **comment)
                cls.object_group_wall_posts.append(wall_post)

        # print('total group wall post', len(cls.object_group_wall_posts))
        assert len(cls.object_group_wall_posts) == len(data_group_wall_posts)

    @classmethod
    def prepare_friends(cls, data_friends):
        for data_friend in data_friends:
            if not Friendship.objects.filter(**data_friend).exists():
                from_user = data_friend['from_user']
                to_user = data_friend['to_user']
                friend = Friendship.objects.create(from_user=from_user, to_user=to_user)
                Friendship.objects.create(from_user=to_user, to_user=from_user)
                cls.object_friends.append(friend)

        # print('total friends', len(cls.object_friends))
        assert len(cls.object_friends) == len(data_friends)

    def login_all_prepared_user(self):
        # prepare users client
        for i, data_user in enumerate(self.data_users):
            data_login = {
                'username': data_user['phone'] or data_user['email'], 'password': data_user['password']
            }
            self.client_users[i].logout()
            response = self.client_users[i].post(self.reversed_urls['login_user'], data_login)
            token = response.data.get("token")
            self.assertNotEqual(token, None, data_login)
            self.client_users[i].credentials(HTTP_AUTHORIZATION="Token " + token)

        for i, data_admin in enumerate(self.data_admins):
            data_login = {
                'username': data_admin['phone'] or data_admin['email'], 'password': data_admin['password']
            }
            self.client_admins[i].logout()
            response = self.client_admins[i].post(self.reversed_urls['login_user'], data_login)
            token = response.data.get("token")
            self.assertNotEqual(token, None, data_login)
            self.client_admins[i].credentials(HTTP_AUTHORIZATION="Token " + token)
