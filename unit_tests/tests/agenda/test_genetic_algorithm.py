from random import choice

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.timezone import now
from rest_framework import status

from agenda.models import Agenda, UserActivities, Activities, AgendaLocationCandidate, AgendaActivity, AgendaHost, \
    AgendaMember
from social_network.models import LocationOpenTime, LocationTime, LocationProfile, GroupProfile
from unit_tests.tests.common import PreparedTestCase

GitaUser = get_user_model()


class GeneticAlgorithmTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()
        self.user_index = 1

    def create_init_user(self):
        self.user_index = 0
        data_user_temp = {
            'username': 'dimasinchidi',
            'email': 'dimasinchidi@test.com',
            'password': 's7r0n9p455',
            'first_name': 'Dimas',
            'last_name': 'Ari',
            'phone': '+628152121216',
            'profile': {
                'birthday': '{}-{}-{}'.format(
                    choice(range(1950, 2010)), choice(range(1, 13)), choice(range(1, 27))),  # YYYY-MM-DD
                'gender': choice(['N', 'F', 'M', 'O']),  # N, F, M, O
            }
        }

        data_profile = data_user_temp.pop('profile')
        phone = data_user_temp.pop('phone')
        password = data_user_temp.pop('password')
        email = data_user_temp.pop('email')
        data_user_temp['email_verified'] = now()
        data_user_temp['phone_verified'] = now()

        try:
            GitaUser.objects.create_gita_user(
                phone, data_profile, password, email, **data_user_temp
            )
        except ValidationError:
            pass
        self.object_users = [
            GitaUser.objects.first()
        ]

    def create_group_profile(self):
        self.create_init_user()
        self.fkti_group = GroupProfile.objects.create(
            group_name='Teknik Informatika FKTI Unmul',
            creator=self.object_users[self.user_index]
        )

    def create_user(self, user_data) -> GitaUser:
        user_phone = f"+62815{str(user_data['id']).zfill(8)}"
        if GitaUser.objects.filter(phone=user_phone).exists():
            instance = GitaUser.objects.get(phone=user_phone)
            return instance
        user_full_name = user_data['first_name'].split()
        user_first_name = ' '.join(user_full_name[:2]) if '.' in user_full_name[0] else user_full_name[0]
        user_last_name = ' '.join(user_full_name[2:]) if '.' in user_full_name[0] else ' '.join(user_full_name[1:])
        return GitaUser.objects.create_gita_user(
            user_phone,
            data_profile={'birthday': '1980-10-10'},
            password='Percobaan123',
            first_name=user_first_name,
            last_name=user_last_name,
        )

    def create_location(self, location_data) -> LocationProfile:
        location_profile = LocationProfile.objects.create(
            creator=self.object_users[self.user_index],
            location_name=location_data['location_name'],
        )
        LocationTime.objects.create(
            location=location_profile,
            open_loop=location_data['time']['open_loop'],
            open_interlude=location_data['time']['open_interlude']
        )
        for open_time in location_data['open_times']:
            LocationOpenTime.objects.create(
                location=location_profile,
                in_loop=open_time['in_loop'],
                time_start=open_time['time_start'],
                time_until=open_time['time_until']
            )
        return location_profile

    def prepare_fkti_data(self):
        # create lecturers user data
        self.lecturer_users = [
            {'first_name': "Almasari Aksenta, M.Eng", 'id': 1},
            {'first_name': "Andi Tejawati, M.Si", 'id': 2},
            {'first_name': "Anton Prafanto, MT", 'id': 3},
            {'first_name': "Bambang Cahyono, MT", 'id': 4},
            {'first_name': "Dr. Fahrul Agus, MT", 'id': 5},
            {'first_name': "Dr. Ita Merni P, M.Si", 'id': 6},
            {'first_name': "Dr. Nataniel Dengen, M.Si", 'id': 7},
            {'first_name': "Dr. Tiopan Henry Manto G, ST, MT", 'id': 8},
            {'first_name': "Edy Budiman, MT", 'id': 9},
            {'first_name': "Gubtha Mahendra Putra, S.Kom, M.Eng", 'id': 10},
            {'first_name': "Hario Jati S, M.Kom", 'id': 11},
            {'first_name': "Haviluddin, Ph.D", 'id': 12},
            {'first_name': "Herman Santoso P, M.PFis", 'id': 13},
            {'first_name': "Islamiyah, S. Kom, M.Kom", 'id': 14},
            {'first_name': "Joan Angelina W, M.Kom", 'id': 15},
            {'first_name': "Masna Wati, S. Si, MT", 'id': 16},
            {'first_name': "Medi Taruk, M. Cs", 'id': 17},
            {'first_name': "Muhammad Bambang F, M.Kom", 'id': 18},
            {'first_name': "Novi Indrayani, M.MT", 'id': 19},
            {'first_name': "Novianti Puspitasari, M.Eng", 'id': 20},
            {'first_name': "Pohny, MT", 'id': 21},
            {'first_name': "Reza Wardhana, M.Eng", 'id': 22},
            {'first_name': "Rosmasari, S.Kom, MT", 'id': 23},
            {'first_name': "Ummul Hairah, MT", 'id': 24},
            {'first_name': "Vina Zahrotun Kamila, M.Kom", 'id': 25},
        ]
        self.lecturer_user_objects = {
            user_data['id']: self.create_user(user_data) for user_data in self.lecturer_users
        }

        # create locations data
        self.candidate_locations = [
            {
                'id': 1,
                'location_name': '405',
                'time': {'open_loop': 'da', 'open_interlude': 1},
                'open_times': [
                    {'in_loop': i, 'time_start': ((7 * 60) + 30) * 60, 'time_until': ((18 * 60) + 0) * 60}
                    for i in range(5)
                ]
            },
            {
                'id': 2,
                'location_name': '406',
                'time': {'open_loop': 'da', 'open_interlude': 1},
                'open_times': [
                    {'in_loop': i, 'time_start': ((7 * 60) + 30) * 60, 'time_until': ((18 * 60) + 0) * 60}
                    for i in range(5)
                ]
            },
            {
                'id': 3,
                'location_name': '407',
                'time': {'open_loop': 'da', 'open_interlude': 1},
                'open_times': [
                    {'in_loop': i, 'time_start': ((7 * 60) + 30) * 60, 'time_until': ((18 * 60) + 0) * 60}
                    for i in range(5)
                ]
            },
            {
                'id': 4,
                'location_name': 'Gedung IKA Lantai 2',
                'time': {'open_loop': 'da', 'open_interlude': 1},
                'open_times': [
                    {'in_loop': i, 'time_start': ((7 * 60) + 30) * 60, 'time_until': ((18 * 60) + 0) * 60}
                    for i in range(5)
                ]
            },
            {
                'id': 5,
                'location_name': 'Gedung MPK Lantai 1',
                'time': {'open_loop': 'da', 'open_interlude': 1},
                'open_times': [
                    {'in_loop': i, 'time_start': ((7 * 60) + 30) * 60, 'time_until': ((18 * 60) + 0) * 60}
                    for i in range(5)
                ]
            },
        ]
        self.candidate_location_objects = {
            location_data['id']: self.create_location(location_data) for location_data in self.candidate_locations
        }

        self.candidate_activities = [
            {'activity_name': "C'18 Sistem Digital", 'description': "Kelas C'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [23, 3, ]},
            {'activity_name': "BD'16 Sistem Pendukung Keputusan", 'description': "Kelas BD'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [16, ]},
            {'activity_name': "A'17 Sistem Manajemen Basis Data", 'description': "Kelas A'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [24, 25, ]},
            {'activity_name': "A'18 Fisika Gelombang dan Elektomagnet", 'description': "Kelas A'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [7, 13, ]},
            {'activity_name': "B'18 Ilmu Sosial dan Budaya Dasar", 'description': "Kelas B'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [2, ]},
            {'activity_name': "A'16 Sistem Pendukung Keputusan", 'description': "Kelas A'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [16, ]},
            {'activity_name': "E'18 Sistem Operasi", 'description': "Kelas E'18 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [21, 10, ]},
            {'activity_name': "D'18 Jaringan dan Keamanan Komputer", 'description': "Kelas D'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [17, 22, ]},
            {'activity_name': "B'18 Jaringan dan Keamanan Komputer", 'description': "Kelas B'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [17, 22, ]},
            {'activity_name': "E'16 Sistem Pendukung Keputusan", 'description': "Kelas E'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [16, ]},
            {'activity_name': "D'17 Sistem Manajemen Basis Data", 'description': "Kelas D'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [24, 25, ]},
            {'activity_name': "AC'16 Interaksi Manusia dan Komputer", 'description': "Kelas AC'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [23, 18, ]},
            {'activity_name': "AB'17 Manajemen Local Area Network", 'description': "Kelas AB'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [17, 19, ]},
            {'activity_name': "BE'16 Pengolahan Citra", 'description': "Kelas BE'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [14, 22, ]},
            {'activity_name': "C'17 Pemrograman Visual", 'description': "Kelas C'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 3, ]},
            {'activity_name': "A'16 Audit Sistem Informasi", 'description': "Kelas A'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 1, ]},
            {'activity_name': "BC'16 Basis Data Terdistribusi", 'description': "Kelas BC'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [24, 17, ]},
            {'activity_name': "D'16 Interaksi Manusia dan Komputer", 'description': "Kelas D'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [23, 18, ]},
            {'activity_name': "D'17 Komputer Masyarakat", 'description': "Kelas D'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 13, 12]},
            {'activity_name': "AE'16 Etika dan Profesionalis Informatika",
             'description': "Kelas AE'16 dengan bobot SKS 3", 'duration': 7200, 'hosts': [15, 20, ]},
            {'activity_name': "C'17 Kriptografi", 'description': "Kelas C'17 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [14, 10, ]},
            {'activity_name': "A'17 Komputer Masyarakat", 'description': "Kelas A'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 13, 12]},
            {'activity_name': "C'17 Pemrograman Framework", 'description': "Kelas C'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 25, ]},
            {'activity_name': "A'18 Jaringan dan Keamanan Komputer", 'description': "Kelas A'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [17, 22, ]},
            {'activity_name': "C'18 Fisika Gelombang dan Elektomagnet", 'description': "Kelas C'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [7, 13, ]},
            {'activity_name': "D'18 Sistem Digital", 'description': "Kelas D'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [23, 3, ]},
            {'activity_name': "A'16 Pengolahan Citra", 'description': "Kelas A'16 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [14, 22, ]},
            {'activity_name': "E'18, Kelas Perbaikan 2017  Ilmu Sosial dan Budaya Dasar",
             'description': "Kelas E'18, Kelas Perbaikan 2017  dengan bobot SKS 3", 'duration': 7200, 'hosts': [2, ]},
            {'activity_name': "D'18 Algoritma dan Pemrograman Lanjut", 'description': "Kelas D'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 20, ]},
            {'activity_name': "B'18 Fisika Gelombang dan Elektomagnet", 'description': "Kelas B'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [7, 13, ]},
            {'activity_name': "B'17 Pemrograman Framework", 'description': "Kelas B'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 25, ]},
            {'activity_name': "B16 Etika dan Profesionalis Informatika", 'description': "Kelas B16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [15, 20, ]},
            {'activity_name': "C'16 Kelas Tambahan  Sistem Pendukung Keputusan",
             'description': "Kelas C'16 Kelas Tambahan  dengan bobot SKS 2", 'duration': 5400, 'hosts': [16, ]},
            {'activity_name': "A-E'18 & Kelas Perbaikan 2017  Metode Numerik",
             'description': "Kelas A-E'18 & Kelas Perbaikan 2017  dengan bobot SKS 3", 'duration': 7200,
             'hosts': [5, 13, ]},
            {'activity_name': "A'17 Teknologi Multimedia", 'description': "Kelas A'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [18, 22, ]},
            {'activity_name': "D'17 Pemrograman Visual", 'description': "Kelas D'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 3, ]},
            {'activity_name': "A'16 Technopreneurship", 'description': "Kelas A'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [1, 6, ]},
            {'activity_name': "CD'16 Etika dan Profesionalis Informatika",
             'description': "Kelas CD'16 dengan bobot SKS 3", 'duration': 7200, 'hosts': [15, 20, ]},
            {'activity_name': "C'17 Manajemen Local Area Network", 'description': "Kelas C'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [17, 19, ]},
            {'activity_name': "B'17 Komputer Masyarakat", 'description': "Kelas B'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 13, 12]},
            {'activity_name': "D'18 Sistem Operasi", 'description': "Kelas D'18 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [21, 10, ]},
            {'activity_name': "B'17 Pemrograman Visual", 'description': "Kelas B'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 3, ]},
            {'activity_name': "DE'16 Audit Sistem Informasi", 'description': "Kelas DE'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 1, ]},
            {'activity_name': "D'17 Animasi Komputer", 'description': "Kelas D'17 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [18, 10, ]},
            {'activity_name': "C'18 Matematika Informatika", 'description': "Kelas C'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [5, 8, ]},
            {'activity_name': "CD'16 Pengolahan Citra", 'description': "Kelas CD'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [14, 22, ]},
            {'activity_name': "E'18 Fisika Gelombang dan Elektomagnet", 'description': "Kelas E'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [7, 13, ]},
            {'activity_name': "B'18 Sistem Digital", 'description': "Kelas B'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [23, 3, ]},
            {'activity_name': "C'18 Ilmu Sosial dan Budaya Dasar", 'description': "Kelas C'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [2, ]},
            {'activity_name': "A'18 Algoritma dan Pemrograman Lanjut", 'description': "Kelas A'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 20, ]},
            {'activity_name': "C'17 Kelas Tambahan  Rekayasa Pengembangan Web",
             'description': "Kelas C'17 Kelas Tambahan  dengan bobot SKS 3", 'duration': 7200, 'hosts': [22, 11, ]},
            {'activity_name': "E'18 Technopreneur", 'description': "Kelas E'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [1, 6, ]},
            {'activity_name': "Kelas Perbaikan 2017  Matematika Diskrit",
             'description': "Kelas Kelas Perbaikan 2017  dengan bobot SKS 3", 'duration': 7200, 'hosts': [8, 4, ]},
            {'activity_name': "D'17 Kecerdasan Buatan", 'description': "Kelas D'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [15, 12, ]},
            {'activity_name': "B'18 Algoritma dan Pemrograman Lanjut", 'description': "Kelas B'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 20, ]},
            {'activity_name': "A'17 Pemrograman Framework", 'description': "Kelas A'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 25, ]},
            {'activity_name': "Kelas Tambahan A'18  Technopreneur",
             'description': "Kelas Kelas Tambahan A'18  dengan bobot SKS 3", 'duration': 7200, 'hosts': [1, 6, ]},
            {'activity_name': "B'17 Sistem Manajemen Basis Data", 'description': "Kelas B'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [24, 25, ]},
            {'activity_name': "C'18. Sistem Operasi", 'description': "Kelas C'18. dengan bobot SKS 2", 'duration': 5400,
             'hosts': [21, 10, ]},
            {'activity_name': "C'17 Teknologi Multimedia", 'description': "Kelas C'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [18, 22, ]},
            {'activity_name': "C'16 Audit Sistem Informasi", 'description': "Kelas C'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 1, ]},
            {'activity_name': "ADE'16 Basis Data Terdistribusi", 'description': "Kelas ADE'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [24, 17, ]},
            {'activity_name': "A'17 Kecerdasan Buatan", 'description': "Kelas A'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [15, 12, ]},
            {'activity_name': "C'16 Konsep Pengembangan E-Learning", 'description': "Kelas C'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [4, ]},
            {'activity_name': "BD'17 Teknologi Multimedia", 'description': "Kelas BD'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [18, 22, ]},
            {'activity_name': "Kelas Perbaikan 2017 Pendidikan Agama Islam",
             'description': "Kelas Kelas Perbaikan 2017 dengan bobot SKS 3", 'duration': 7200, 'hosts': []},
            {'activity_name': "D'18 Fisika Gelombang dan Elektomagnet", 'description': "Kelas D'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [7, 13, ]},
            {'activity_name': "BC'17 Animasi Komputer", 'description': "Kelas BC'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [18, 10, ]},
            {'activity_name': "C'18 Technopreneur", 'description': "Kelas C'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [1, 6, ]},
            {'activity_name': "A'18 Sistem Digital", 'description': "Kelas A'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [23, 3, ]},
            {'activity_name': "E'18 Matematika Informatika", 'description': "Kelas E'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [5, 8, ]},
            {'activity_name': "C'17 Sistem Manajemen Basis Data", 'description': "Kelas C'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [24, 25, ]},
            {'activity_name': "C'18 Algoritma dan Pemrograman Lanjut", 'description': "Kelas C'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 20, ]},
            {'activity_name': "D'18 Technopreneur", 'description': "Kelas D'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [1, 6, ]},
            {'activity_name': "A'18 Ilmu Sosial dan Budaya Dasar", 'description': "Kelas A'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [2, ]},
            {'activity_name': "B'18 Matematika Informatika", 'description': "Kelas B'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [5, 8, ]},
            {'activity_name': "E'18 Algoritma dan Pemrograman Lanjut", 'description': "Kelas E'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 20, ]},
            {'activity_name': "C'17 Kecerdasan Buatan", 'description': "Kelas C'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [15, 12, ]},
            {'activity_name': "B'16 Audit Sistem Informasi", 'description': "Kelas B'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 1, ]},
            {'activity_name': "B'18, Kelas  Perbaikan 2017 Sistem Operasi",
             'description': "Kelas B'18, Kelas  Perbaikan 2017 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [21, 10, ]},
            {'activity_name': "B'17 Kecerdasan Buatan", 'description': "Kelas B'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [15, 12, ]},
            {'activity_name': "B'16 Technopreneurship", 'description': "Kelas B'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [1, 6, ]},
            {'activity_name': "Kelas Perbaikan 2017 Teori Graph dan Otomata",
             'description': "Kelas Kelas Perbaikan 2017 dengan bobot SKS 3", 'duration': 7200, 'hosts': [16, 14, ]},
            {'activity_name': "C'18 Jaringan dan Keamanan Komputer", 'description': "Kelas C'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [17, 22, ]},
            {'activity_name': "E'16 Interaksi Manusia dan Komputer", 'description': "Kelas E'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [23, 18, ]},
            {'activity_name': "C'17 Komputer Masyarakat", 'description': "Kelas C'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 13, 12]},
            {'activity_name': "D'17 Pemrograman Framework", 'description': "Kelas D'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [11, 25, ]},
            {'activity_name': "E'18 Jaringan dan Keamanan Komputer", 'description': "Kelas E'18 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [17, 22, ]},
            {'activity_name': "B'17 Kriptografi", 'description': "Kelas B'17 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [14, 10, ]},
            {'activity_name': "AB'16 Konsep Pengembangan E-Learning", 'description': "Kelas AB'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [4, ]},
            {'activity_name': "A'17 Pemrograman Visual", 'description': "Kelas A'17 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [9, 3, ]},
            {'activity_name': "A-D'17 Pemograman Berbasis Jaringan", 'description': "Kelas A-D'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [19, 10, ]},
            {'activity_name': "D'18 Ilmu Sosial dan Budaya Dasar", 'description': "Kelas D'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [2, ]},
            {'activity_name': "CDE'16 Technopreneurship", 'description': "Kelas CDE'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [1, 6, ]},
            {'activity_name': "A'18 Matematika Informatika", 'description': "Kelas A'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [5, 8, ]},
            {'activity_name': "E'18 Sistem Digital", 'description': "Kelas E'18 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [23, 3, ]},
            {'activity_name': "B'17 Rekayasa Pengembangan Web", 'description': "Kelas B'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [22, 11, ]},
            {'activity_name': "B16 Interaksi Manusia dan Komputer", 'description': "Kelas B16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [23, 18, ]},
            {'activity_name': "Kelas Tambahan B'18 Technopreneur",
             'description': "Kelas Kelas Tambahan B'18 dengan bobot SKS 3", 'duration': 7200, 'hosts': [1, 6, ]},
            {'activity_name': "D'18 Matematika Informatika", 'description': "Kelas D'18 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [5, 8, ]},
            {'activity_name': "AD'17 Kriptografi", 'description': "Kelas AD'17 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [14, 10, ]},
            {'activity_name': "Kelas Perbaikan 2017 Pendidikan Agama Kristen Katolik",
             'description': "Kelas Kelas Perbaikan 2017 dengan bobot SKS 3", 'duration': 7200, 'hosts': []},
            {'activity_name': "AD'17 Rekayasa Pengembangan Web", 'description': "Kelas AD'17 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [22, 11, ]},
            {'activity_name': "D'16 Manajemen Local Area Network", 'description': "Kelas D'16 dengan bobot SKS 3",
             'duration': 7200, 'hosts': [17, 19, ]},
            {'activity_name': "A'18 Sistem Operasi", 'description': "Kelas A'18 dengan bobot SKS 2", 'duration': 5400,
             'hosts': [21, 10, ]},
            {'activity_name': "A'17 Animasi Komputer", 'description': "Kelas A'17 dengan bobot SKS 3", 'duration': 7200,
             'hosts': [18, 10, ]},
            {'activity_name': "Kelas Perbaikan 2017 Struktur Data",
             'description': "Kelas Kelas Perbaikan 2017 dengan bobot SKS 2", 'duration': 5400, 'hosts': [20, 23, ]},
            {'activity_name': "DE'16 Konsep Pengembangan E-Learning", 'description': "Kelas DE'16 dengan bobot SKS 2",
             'duration': 5400, 'hosts': [4, ]},
        ]

    def test_create_fkti_agenda(self):
        self.prepare_fkti_data()
        validated_data = {
            'agenda_name': 'Jadwal Semester Genap TI 2018-2019',
            'description': 'Jadwal kuliah semester genap jurusan teknik informatika '
                           'tahun akademik 2018/2019 disusun secara otomatis dengan algoritma genetika',
            'loop': Agenda.WEEKLY,
            'loop_interlude': 1,  # equals with every week
            'loop_count': 1,  # 21 meetings
            'range_time_start': '{}-{}-{}T00:00:00+07:00'.format(2019, str(9).zfill(2), str(2).zfill(2)),
            'locations': [
                {'location': LocationProfile.objects.get(
                    id=self.candidate_location_objects[location_data['id']].id)} for location_data in
                self.candidate_locations
            ],

            'activities': [
                {
                    'activity_name': activity['activity_name'],
                    'description': activity['description'],
                    'duration': activity['duration'],
                    'hosts': [
                        {'host': GitaUser.objects.get(
                            id=self.lecturer_user_objects[host].id)} for host in activity['hosts']
                    ],
                } for activity in self.candidate_activities
            ],
        }
        locations_data = validated_data.pop('locations')
        activities_data = validated_data.pop('activities')

        agenda = Agenda.objects.create(
            creator=self.object_users[self.user_index],
            user=validated_data.get('user', None),
            group=self.fkti_group,
            agenda_name=validated_data['agenda_name'],
            description=validated_data.get('description', ''),
            loop=validated_data.get('loop', Agenda.NO_LOOP),
            loop_interlude=validated_data.get('loop_interlude', 0),
            range_time_start=validated_data['range_time_start'],
            range_time_until=validated_data.get('range_time_until', None),
            loop_count=validated_data.get('loop_count', 0),
        )

        AgendaLocationCandidate.objects.bulk_create([
            AgendaLocationCandidate(
                agenda=agenda, location=location_data['location']
            ) for location_data in locations_data
        ])

        for activity_data in activities_data:
            hosts_data = activity_data.pop('hosts', [])
            members_data = activity_data.pop('members', [])
            agenda_activity = AgendaActivity.objects.create(
                agenda=agenda,
                activity_name=activity_data['activity_name'],
                description=activity_data.get('description', ''),
                duration=activity_data['duration'],
                location=activity_data.get('location', None),
            )

            AgendaHost.objects.bulk_create([
                AgendaHost(
                    agenda_activity=agenda_activity,
                    host=host_data['host']
                ) for host_data in hosts_data
            ])
            AgendaMember.objects.bulk_create([
                AgendaMember(
                    agenda_activity=agenda_activity,
                    member=member_data['member']
                ) for member_data in members_data
            ])
        # response = self.client_users[self.user_index].post(
        #     reverse('agenda-group', kwargs={'group_id': self.fkti_group.id}),
        #     group_repeat_agenda_data
        # )
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return {'id': agenda.id}

    def test_run_genetic_algorithm(self):
        response_data = self.test_create_fkti_agenda()

        agenda = Agenda.objects.get(id=response_data['id'])
        agenda.generate_activity(True)
        agenda = Agenda.objects.get(id=response_data['id'])
        if agenda.is_valid:
            # check is all activity valid
            self.assertFalse(
                UserActivities.objects.filter(
                    activity__agenda_activity__agenda=agenda,
                    is_activity_valid=False
                ).exists()
            )
            for user_activities in UserActivities.objects.filter(
                    activity__agenda_activity__agenda=agenda
            ):
                self.assertTrue(
                    7 < user_activities.activity.time_start.hour < 18,
                    user_activities.activity.time_start
                )
            for a in Activities.objects.filter(agenda_activity__agenda=agenda):
                print(a.activity_name, a.time_start, a.time_until)

    def test_generate_fkti_agenda(self):
        response_data = self.test_create_fkti_agenda()

        response = self.client_users[self.user_index].get(
            reverse('agenda-generate_group', kwargs={'group_id': self.fkti_group.id, 'agenda_id': response_data['id']})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
