from django.urls import reverse
from unittest import skip
from rest_framework import status

from agenda.models import Agenda
from unit_tests.tests.common import PreparedTestCase


class AgendaGroupTest(PreparedTestCase):

    def setUp(self):
        self.login_all_prepared_user()

    def read_group_agendas(self, user_index, group_index):
        response = self.client_users[user_index].get(
            reverse('agenda-group', kwargs={'group_id': self.object_groups[group_index].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        return response.data

    def test_create_group_once_agenda(self):
        user_index = 4
        group_index = 0
        group_once_agenda_data = {
            'agenda_name': 'Group One-Time Agenda',
            'description': 'Group agenda that will only generated once or not repeat in specific range time',
            'loop': Agenda.NO_LOOP,
            'range_time_start': '{}-{}-{}T09:00:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            'range_time_until': '{}-{}-{}T16:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),
            'activities': (
                {
                    'activity_name': 'Rapat',
                    'description': 'Rapat mendadak, ngumpul dulu',
                    'duration': 3600,
                    'hosts': (
                        {'host': self.object_users[0].id},
                    ),
                },
            ),
        }
        response = self.client_users[user_index].post(
            reverse('agenda-group', kwargs={'group_id': self.object_groups[group_index].id}),
            group_once_agenda_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return user_index, group_index, response.data

    def test_create_group_repeat_agenda(self):
        user_index = 4
        group_index = 0
        group_repeat_agenda_data = {
            'agenda_name': 'Group Repeat Agenda',
            'description': 'Group agenda that will generated multiple times in multiple time range',
            'loop': Agenda.WEEKLY,
            'loop_interlude': 2,  # equals with every two week
            'loop_count': 4,  # equals with in between 90 days, do it 4 times
            'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            # 'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
            #     self.tomorrow.year, str(self.tomorrow.month).zfill(2), (self.tomorrow + timedelta(days=90)).day
            # ),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),

            'activities': (
                {
                    'activity_name': 'Badminton',
                    'description': 'Latih respon, badminton bareng dulu',
                    'duration': 3600,  # 10 minutes
                    'hosts': (
                        {'host': self.object_users[0].id},
                    ),
                },
            ),
        }
        response = self.client_users[user_index].post(
            reverse('agenda-group', kwargs={'group_id': self.object_groups[group_index].id}),
            group_repeat_agenda_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_read_group_agendas(self):
        user_index = 4
        group_index = 0
        response = self.client_users[user_index].get(
            reverse('agenda-group', kwargs={'group_id': self.object_groups[group_index].id}),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_update_group_agenda(self):
        user_index, group_index, agenda_data = self.test_create_group_once_agenda()
        agenda_update_data = {
            'agenda_name': 'Group One-Time Agenda Updated',
            'description': 'Group agenda that will only generated once or not repeat in specific range time updated',
            'loop': Agenda.NO_LOOP,
            'range_time_start': '{}-{}-{}T06:00:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            'range_time_until': '{}-{}-{}T12:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'locations': [
                {'location': self.object_locations[0].id},
            ],
            'activities': [
                {
                    'activity_name': 'Rapat updated',
                    'description': 'Rapat mendadak, ngumpul dulu updated',
                    'duration': 1800,
                    'hosts': (
                        {'host': self.object_users[0].id},
                    ),
                },
            ],
        }
        response = self.client_users[user_index].patch(
            reverse(
                'agenda-detail_group',
                kwargs={'group_id': self.object_groups[group_index].id, 'agenda_id': agenda_data['id']}
            ),
            agenda_update_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['agenda_name'],
            agenda_update_data['agenda_name']
        )
        self.assertEqual(
            response.data['activities'][0]['activity_name'],
            agenda_update_data['activities'][0]['activity_name']
        )

    def test_delete_group_agenda(self):
        user_index, group_index, agenda_data = self.test_create_group_once_agenda()
        response = self.client_users[user_index].delete(
            reverse(
                'agenda-detail_group',
                kwargs={'group_id': self.object_groups[group_index].id, 'agenda_id': agenda_data['id']}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertEqual(
            Agenda.objects.filter(
                group=self.object_groups[group_index],
                agenda_name=agenda_data['agenda_name']
            ).count(),
            0
        )
        response_data = self.read_group_agendas(user_index, group_index)
        self.assertEqual(response_data['count'], 0)
