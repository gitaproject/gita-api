from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status

from social_network.models.verification import UserPhoneVerificationToken
from unit_tests.tests.common import PreparedTestCase

User = get_user_model()


class RegistrationTest(PreparedTestCase):
    def test_registration_check_username(self):
        for user in User.objects.all():
            check_username_data = {'username': user.username}
            response = self.client_anonymous.post(reverse('registration-check_username'), check_username_data)
            self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
            self.assertFalse(response.data['is_valid'], response.data)
        check_username_data = {'username': 'unique_username'}
        response = self.client_anonymous.post(reverse('registration-check_username'), check_username_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertTrue(response.data['is_valid'], response.data)

    def test_registration_register(self):
        register_data = {
            'username': 'new_user',
            'phone': '+628123456789',
            'first_name': 'New',
            'last_name': 'User',
            'password': 'Pr3tt15tr0n9',
            'birthday': '1994-12-25'
        }
        response = self.client_anonymous.post(reverse('registration-register'), register_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertRaises(KeyError, lambda: response.data['password'])
        self.assertTrue(UserPhoneVerificationToken.objects.filter(user__username=register_data['username']).exists())
        return response.data

    def test_registration_resend_token(self):
        register_data = self.test_registration_register()
        resend_token_data = {
            'phone': register_data['phone'],
        }
        response = self.client_anonymous.post(reverse('registration-resend_token'), resend_token_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertIn('Please wait', str(response.data), response.data)

    def test_registration_verify_phone(self):
        register_data = self.test_registration_register()
        user = User.objects.get(username=register_data['username'])
        resend_token_data = {
            'phone': register_data['phone'],
            'phone_token': user.phone_verification_token.token
        }
        response = self.client_anonymous.post(reverse('registration-verify_phone'), resend_token_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        return response.data['phone_key']
