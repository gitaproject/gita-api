from django.contrib.auth import get_user_model
from django.core import mail
from django.test import override_settings
from django.urls import reverse
from rest_framework import status

from social_network.models.verification import UserForgotPasswordToken
from unit_tests.tests.common import PreparedTestCase

User = get_user_model()


class AccountRetrievalTest(PreparedTestCase):
    @override_settings(EMAIL_BACKEND='anymail.backends.test.EmailBackend', IS_ONLINE=True)
    def test_account_recovery(self):
        account_recovery_data = {
            'username': self.data_users[0]['phone']
        }
        response = self.client_anonymous.post(reverse('account_recovery'), account_recovery_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(len(mail.outbox), 1, response.data)
        self.assertEqual(mail.outbox[0].subject, "Account Recovery | GiTa")
        self.assertEqual(mail.outbox[0].to, [self.data_users[0]['email']])
        forgot_password_token = UserForgotPasswordToken.objects.get(user__username=self.data_users[0]['username'])
        self.assertIn(forgot_password_token.token, mail.outbox[0].body)
        self.assertIn(forgot_password_token.uuid.hex, mail.outbox[0].body)
        return {
            'token': forgot_password_token.token,
            'uuid': forgot_password_token.uuid.hex,
            'username': self.data_users[0]['username']
        }

    def test_validate_recovery(self):
        account_recovery_data = self.test_account_recovery()
        validate_recovery_data = {
            'uuid': account_recovery_data['uuid'],
            'token': account_recovery_data['token'],
        }
        response = self.client_anonymous.post(
            reverse('validate_account_retrieval'), validate_recovery_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['is_valid'])

    def test_reset_password(self):
        account_recovery_data = self.test_account_recovery()
        password = 'Ultra5trongN3wP455!'
        reset_password_data = {
            'uuid': account_recovery_data['uuid'],
            'token': account_recovery_data['token'],
            'password': password,
        }
        response = self.client_anonymous.post(
            reverse('recovery_set_password'), reset_password_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.get(username=account_recovery_data['username'])
        self.assertTrue(user.check_password(password))
