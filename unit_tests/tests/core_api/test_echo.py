from django.urls import reverse
from rest_framework import status

from unit_tests.tests.common import PreparedTestCase


class EchoTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_echo_public(self):
        echo_data = {
            'message': 'public test echo'
        }
        response = self.client_anonymous.post(
            reverse('echo_public'),
            echo_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(response.data['message'], echo_data['message'], response.data)
        self.assertEqual(response.data['status'], 'Echo public test success.', response.data)

    def test_echo_user(self):
        echo_data = {
            'message': 'user test echo'
        }
        response = self.client_users[0].post(
            reverse('echo_user'),
            echo_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(response.data['message'], echo_data['message'], response.data)
        self.assertEqual(response.data['status'], 'Echo user test success.', response.data)

    def test_echo_admin(self):
        echo_data = {
            'message': 'admin test echo'
        }
        response = self.client_admins[0].post(
            reverse('echo_admin'),
            echo_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(response.data['message'], echo_data['message'], response.data)
        self.assertEqual(response.data['status'], 'Echo admin test success.', response.data)
