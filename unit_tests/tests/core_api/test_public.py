from django.urls import reverse
from rest_framework import status

from unit_tests.tests.common import PreparedTestCase


class PublicTest(PreparedTestCase):
    def test_faq(self):
        response = self.client_anonymous.get(reverse('faq'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_tos(self):
        response = self.client_anonymous.get(reverse('tos'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
