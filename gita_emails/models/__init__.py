from django.contrib.auth import get_user_model
from django.db import models

from core_api.abstracts.base import BaseAbstractModel

User = get_user_model()


class EmailHistory(BaseAbstractModel):
    FAIL = 1
    SUCCESS = 0
    STATUS = (
        (FAIL, 'Fail'),
        (SUCCESS, 'Success'),
    )
    message = models.TextField()
    status = models.SmallIntegerField(choices=STATUS, default=FAIL)


class EmailReceivers(BaseAbstractModel):
    email = models.ForeignKey(EmailHistory, models.CASCADE, 'receivers')
    receiver = models.EmailField()
