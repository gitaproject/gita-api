from django.apps import AppConfig


class GitaEmailsConfig(AppConfig):
    name = 'gita_emails'
