from rest_framework import generics, permissions

from notification.models.notification import Notification
from notification.serializers.notification import NotificationSerializer


class NotificationPersonalAPIView(generics.ListAPIView):
    serializer_class = NotificationSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user)
