# Generated by Django 2.1.5 on 2019-02-09 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0002_auto_20190125_2004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activities',
            name='description',
            field=models.TextField(blank=True, default='', max_length=255),
        ),
    ]
