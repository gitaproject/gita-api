# Generated by Django 2.1.5 on 2019-01-25 13:04

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('duration', models.BigIntegerField(blank=True)),
                ('time_start', models.DateTimeField()),
                ('time_until', models.DateTimeField(blank=True)),
                ('activity_name', models.CharField(max_length=64)),
                ('description', models.TextField(default='', max_length=255)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Agenda',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('agenda_name', models.CharField(max_length=64)),
                ('description', models.TextField(blank=True, default='', max_length=255)),
                ('loop', models.SmallIntegerField(choices=[(0, 'Once'), (1, 'Secondly'), (2, 'Minutely'), (3, 'Hourly'), (4, 'Daily'), (5, 'Weekly'), (6, 'Monthly'), (7, 'Yearly')], default=0)),
                ('loop_interlude', models.PositiveIntegerField(blank=True, default=0)),
                ('range_time_start', models.DateTimeField()),
                ('range_time_until', models.DateTimeField(blank=True)),
                ('loop_count', models.PositiveIntegerField(blank=True, default=0)),
                ('is_valid', models.BooleanField(blank=True, default=False)),
                ('published', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='AgendaActivity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('activity_name', models.CharField(max_length=64)),
                ('description', models.TextField(blank=True, default='', max_length=255)),
                ('duration', models.PositiveIntegerField()),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='AgendaHost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='AgendaLocationCandidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='AgendaMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='InvitationActivities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('rejected', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='UserActivities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_activity_valid', models.BooleanField(blank=True, default=True)),
                ('activity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agenda.Activities')),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('super_objects', django.db.models.manager.Manager()),
            ],
        ),
    ]
