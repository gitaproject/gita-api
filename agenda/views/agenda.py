from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions
from rest_framework.response import Response

from agenda.models import Agenda
from agenda.serializers.agenda import AgendaSerializer
from core_api.permissions import IsDefaultAuthenticatedUser
from social_network.models import GroupProfile


class PersonalAgendaAPIView(generics.ListCreateAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return Agenda.objects.filter(user=self.request.user).order_by('-created_at')

    def perform_create(self, serializer):
        serializer.save(
            user=self.request.user,
            creator=self.request.user,
        )


class PersonalAgendaDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'agenda_id'

    def get_object(self):
        if not Agenda.objects.filter(id=self.kwargs.get(self.lookup_url_kwarg)).exists():
            raise exceptions.ValidationError(
                _('Agenda with id {} does not exist').format(self.kwargs.get(self.lookup_url_kwarg)),
                f'detail_agenda_personal_invalid_{self.lookup_url_kwarg}'
            )

        obj = Agenda.objects.get(id=self.kwargs.get(self.lookup_url_kwarg))
        self.check_object_permissions(self.request, obj)
        return obj


class PersonalAgendaGenerateAPIView(generics.RetrieveAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'agenda_id'

    def get_queryset(self):
        return Agenda.objects.filter(user=self.request.user).order_by('-created_at')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.generate_activity()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class GroupAgendaAPIView(generics.ListCreateAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'group_id'

    def get_queryset(self):
        group = self.get_object()
        return Agenda.objects.filter(group=group).order_by('-created_at')

    def perform_create(self, serializer):
        group = self.get_object()
        serializer.save(
            group=group,
            creator=self.request.user,
        )

    def get_object(self):
        if not GroupProfile.objects.filter(id=self.kwargs[self.lookup_url_kwarg]).exists():
            raise exceptions.ValidationError(
                _('Group with this id does not exists.'),
                f"create_read_agenda_group_invalid_{self.lookup_url_kwarg}"
            )
        obj = GroupProfile.objects.get(id=self.kwargs[self.lookup_url_kwarg])
        self.check_object_permissions(self.request, obj)
        return obj


class GroupAgendaDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    lookup_url_kwarg = 'agenda_id'

    def get_object(self):
        if not Agenda.objects.filter(id=self.kwargs.get(self.lookup_url_kwarg)).exists():
            raise exceptions.ValidationError(
                _('Agenda with id {} does not exist').format(self.kwargs.get(self.lookup_url_kwarg)),
                f'detail_agenda_group_invalid_{self.lookup_url_kwarg}'
            )

        obj = Agenda.objects.get(id=self.kwargs.get(self.lookup_url_kwarg))
        self.check_object_permissions(self.request, obj)
        return obj


class GroupAgendaGenerateAPIView(generics.RetrieveAPIView):
    serializer_class = AgendaSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    queryset = Agenda.objects.all()
    lookup_url_kwarg = 'agenda_id'

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
