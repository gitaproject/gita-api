from rest_framework import generics


class UserScheduleAPIView(generics.ListCreateAPIView): pass


class GroupScheduleAPIView(generics.ListCreateAPIView): pass


class LocationScheduleAPIView(generics.ListCreateAPIView): pass
