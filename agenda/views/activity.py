from datetime import timedelta

from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions
from rest_framework.response import Response

from agenda.models import UserActivities
from agenda.serializers.activity import UserActivitiesSerializer, UserActivityCheckSerializer
from core_api.permissions import IsDefaultAuthenticatedUser


class PersonalActivityAPIView(generics.ListCreateAPIView):
    serializer_class = UserActivitiesSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return UserActivities.objects.filter(
            user=self.request.user,
            activity__time_until__gte=timezone.now()
        ).order_by('activity__time_start')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_object(self):
        return True


class PersonalActivityDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserActivitiesSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    lookup_field = 'id'
    lookup_url_kwarg = 'activity_id'

    def get_object(self):
        if not UserActivities.objects.filter(id=self.kwargs.get(self.lookup_url_kwarg)).exists():
            raise exceptions.ValidationError(
                _('User activity with id {} does not exist').format(self.kwargs.get(self.lookup_url_kwarg)),
                f'detail_personal_activity_invalid_{self.lookup_url_kwarg}'
            )

        obj = UserActivities.objects.get(id=self.kwargs.get(self.lookup_url_kwarg))
        self.check_object_permissions(self.request, obj)
        return obj


class PersonalActivityCheckAPIView(generics.CreateAPIView):
    serializer_class = UserActivityCheckSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = self.get_invalid_activities(queryset, serializer.validated_data)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return UserActivities.objects.filter(user=self.request.user).order_by('activity__time_start')

    @staticmethod
    def get_invalid_activities(queryset, validated_data):
        if validated_data.get('except_id', False):
            queryset = queryset.exclude(id=validated_data['except_id'])
        time_start = validated_data['activity']['time_start']
        time_until = validated_data['activity']['time_start'] + timedelta(
            seconds=validated_data['activity']['duration']
        )
        q_objects = Q(
            activity__time_start__lte=time_start,
            activity__time_until__gte=time_start
        )
        q_objects |= Q(
            activity__time_start__lte=time_until,
            activity__time_until__gte=time_until
        )
        q_objects |= Q(
            activity__time_start__gte=time_start,
            activity__time_until__lte=time_until
        )
        return queryset.filter(q_objects)


class GroupActivityAPIView(generics.ListCreateAPIView):
    serializer_class = UserActivitiesSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return UserActivities.objects.filter(
            user=self.request.user,
            activity__time_start__gte=timezone.now()
        ).order_by('activity__time_start')

    def perform_create(self, serializer):
        serializer.save()

    def get_object(self):
        return


class GroupActivityDetailAPIView(generics.RetrieveAPIView):
    serializer_class = UserActivitiesSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)


class GroupActivityCheckAPIView(generics.CreateAPIView):
    serializer_class = UserActivityCheckSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        queryset = self.filter_queryset(self.get_queryset())

        queryset = self.get_invalid_activities(queryset, serializer.validated_data)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return UserActivities.objects.filter(user=self.request.user).order_by('activity__time_start')

    @staticmethod
    def get_invalid_activities(queryset, validated_data):
        time_start = validated_data['activity'].time_start
        time_until = validated_data['activity'].time_start + timedelta(seconds=validated_data['activity'].duration)
        q_objects = Q(
            activity__time_start__lte=time_start,
            activity__time_until__gte=time_start
        )
        q_objects |= Q(
            activity__time_start__lte=time_until,
            activity__time_until__gte=time_until
        )
        q_objects |= Q(
            activity__time_start__gte=time_start,
            activity__time_until__lte=time_until
        )
        return queryset.filter(q_objects)
